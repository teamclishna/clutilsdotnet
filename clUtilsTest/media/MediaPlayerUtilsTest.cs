﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.log;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace clUtils.media
{
    [TestClass]
    public class MediaPlayerUtilsTest
    {
        [TestMethod]
        public void DetectWMAFile()
        {
            var logger = LoggerFactory.CreateLogger(LoggerName.ConsoleLogger);
            var fi = new FileInfo(@"D:\Data\OneDrive\Music\２Ｓｈｙ４Ｕ\ダイナマイトヘブン\ダイナマイトへブン.wma");
            var result = MediaPlayerUtils.DetectWMAFile(fi, logger);

            return;
        }

        [TestMethod]
        public void DetectMP3File_UTF8()
        {
            var logger = LoggerFactory.CreateLogger(LoggerName.ConsoleLogger);
            var fi = new FileInfo(@"D:\Data\OneDrive\Music\mp3\天誅\02_cyberspace_ecstasy.mp3");
            var result = MediaPlayerUtils.DetectMP3File(fi, logger);

            return;
        }

        [TestMethod]
        public void DetectMP3File_SJIS()
        {
            var logger = LoggerFactory.CreateLogger(LoggerName.ConsoleLogger);
            var fi = new FileInfo(@"D:\Data\OneDrive\Music\ANIMEGIRL\MONSTER MARKET\05 ツバサ.mp3");
            var result = MediaPlayerUtils.DetectMP3File(fi, logger);

            return;
        }
    }
}
