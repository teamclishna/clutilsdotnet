﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace clUtils.io
{
    [TestClass]
    public class FileUtilTest
    {
        [TestMethod]
        public void GetAbsolutePath_正常系()
        {
            var result = FileUtil.GetAbsolutePath(@"..\test.txt", @"C:\temp\dir\base.txt");
            Assert.AreEqual(result, @"C:\temp\test.txt", "絶対パスの生成");
        }
    }
}
