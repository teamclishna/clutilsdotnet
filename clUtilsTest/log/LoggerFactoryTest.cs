﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace clUtils.log
{
    /// <summary>
    /// ロガー生成に関するテスト
    /// </summary>
    [TestClass]
    public class LoggerFactoryTest
    {
        [TestMethod]
        public void 出力テスト()
        {
            LogTest(Create(LoggerName.StdOutLogger));
            LogTest(Create(LoggerName.StdErrLogger));
            LogTest(Create(LoggerName.ConsoleLogger));
        }

        [TestMethod]
        public void ヘッダとかを出したくないテスト()
        {
            var logger = Create(LoggerName.StdOutLogger);
            logger.ShowCurrentLine = false;
            logger.ShowLogLevel = false;
            logger.ShowTimestamp = false;
            logger.ShowCallStackIndentation = false;
            logger.Print(LogLevel.INFO, "出力文字列");

            logger.PrintPush(LogLevel.INFO, "スタック開始(1段目)");
            logger.PrintPush(LogLevel.VERBOSE, "スタック開始(2段目)");
            logger.PrintPush(LogLevel.VERBOSE, "スタック開始(3段目)");
            logger.Print(LogLevel.INFO, "3段目スタック内の出力");
            logger.PrintPop();
            logger.Print(LogLevel.INFO, "2段目スタック内の出力 その1");
            logger.Print(LogLevel.INFO, "2段目スタック内の出力 その2");
            logger.PrintPop();
            logger.PrintPop();
        }

        [TestMethod]
        public void 全部入りの出力テスト()
        {
            var logger = Create(LoggerName.StdOutLogger);
            logger.ShowCurrentLine = true;
            logger.ShowLogLevel = true;
            logger.ShowTimestamp = true;
            logger.ShowCallStackIndentation = true;
            logger.IndentString = "| ";
            logger.Print(LogLevel.INFO, "出力文字列");

            logger.PrintPush(LogLevel.INFO, "スタック開始(1段目)");
            logger.PrintPush(LogLevel.VERBOSE, "スタック開始(2段目)");
            logger.PrintPush(LogLevel.VERBOSE, "スタック開始(3段目)");
            logger.Print(LogLevel.INFO, "3段目スタック内の出力");
            logger.PrintPop();
            logger.Print(LogLevel.INFO, "2段目スタック内の出力 その1");
            logger.Print(LogLevel.INFO, "2段目スタック内の出力 その2");
            logger.PrintPop();
            logger.PrintPop();
        }

        private void LogTest(ILogger logger)
        {
            logger.Print(LogLevel.ERROR, "エラー");
            logger.Print(LogLevel.WARN, "警告");
            logger.Print(LogLevel.INFO, "情報");
            logger.Print(LogLevel.DEBUG, "デバッグ");
            logger.Print(LogLevel.VERBOSE, "詳細な情報");

            logger.PrintPush(LogLevel.INFO, "スタック開始(1段目)");
            logger.PrintPush(LogLevel.VERBOSE, "スタック開始(2段目)");
            logger.PrintPop();
            logger.PrintPop();
        }

        private ILogger Create(string loggerClass)
        {
            return LoggerFactory.CreateLogger(
                loggerClass,
                new LogLevel[] {
                    LogLevel.ERROR,
                    LogLevel.WARN,
                    LogLevel.INFO,
                    LogLevel.DEBUG,
                    LogLevel.VERBOSE
                });
        }
    }
}
