﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace clUtils.util
{
    [TestClass]
    public class MimeTimeTest
    {
        [TestMethod]
        public void DateTime2MimeTime()
        {
            // ミケネコ研究所のサンプル
            Assert.AreEqual(
                "K026S8c9",
                (new DateTime(2002, 6, 27, 8, 37, 9)).ToMikeTime());
        }

        [TestMethod]
        public void MimeTime2DateTime()
        {
            // ミケネコ研究所のサンプル
            Assert.AreEqual(
                MimeTime.FromMikeTime("K026S8c9"),
                new DateTime(2002, 6, 27, 8, 37, 9));
        }

        [TestMethod]
        public void MimeTime2DateTime_エラー_null()
        {
            try
            { MimeTime.FromMikeTime(null); }
            catch(ArgumentException eArg)
            { Assert.AreEqual("invalid MikeTime value: (null)", eArg.Message); }
            catch(Exception eUnknown)
            { Assert.Fail(string.Format("想定外の例外スロー: {0}", eUnknown.ToString())); }
        }

        [TestMethod]
        public void MimeTime2DateTime_エラー_空文字()
        {
            try
            { MimeTime.FromMikeTime(""); }
            catch (ArgumentException eArg)
            { Assert.AreEqual("invalid MikeTime value: \"\"", eArg.Message); }
            catch (Exception eUnknown)
            { Assert.Fail(string.Format("想定外の例外スロー: {0}", eUnknown.ToString())); }
        }

        [TestMethod]
        public void MimeTime2DateTime_エラー_長すぎる文字列()
        {
            try
            { MimeTime.FromMikeTime("K026S8c90"); }
            catch (ArgumentException eArg)
            { Assert.AreEqual("invalid MikeTime value: \"K026S8c90\"", eArg.Message); }
            catch (Exception eUnknown)
            { Assert.Fail(string.Format("想定外の例外スロー: {0}", eUnknown.ToString())); }
        }

        [TestMethod]
        public void MimeTime2DateTime_エラー_変換対象外の文字を含む()
        {
            try
            { MimeTime.FromMikeTime("x026S8c9"); }
            catch (ArgumentException eArg)
            { Assert.AreEqual("invalid MikeTime value: \"x026S8c9\"", eArg.Message); }
            catch (Exception eUnknown)
            { Assert.Fail(string.Format("想定外の例外スロー: {0}", eUnknown.ToString())); }
        }
    }
}
