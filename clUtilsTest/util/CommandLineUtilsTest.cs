﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace clUtils.util
{
    [TestClass]
    public class CommandLineUtilsTest
    {
        [TestMethod]
        public void 単一文字列のパース_単一文字列を配列に変換()
        {
            var result = CommandLineUtils.ParseCommandLineArgs("this is commandline");
            Assert.AreEqual(result.Length, 3, "パースした配列の要素数");
            Assert.AreEqual(result[0], "this", "パース後の要素");
            Assert.AreEqual(result[1], "is", "パース後の要素");
            Assert.AreEqual(result[2], "commandline", "パース後の要素");
        }

        [TestMethod]
        public void 単一文字列のパース_連続したスペースは無視する()
        {
            var result = CommandLineUtils.ParseCommandLineArgs("this is  commandline");
            Assert.AreEqual(result.Length, 3, "パースした配列の要素数");
            Assert.AreEqual(result[0], "this", "パース後の要素");
            Assert.AreEqual(result[1], "is", "パース後の要素");
            Assert.AreEqual(result[2], "commandline", "パース後の要素");
        }

        [TestMethod]
        public void 単一文字列のパース_先頭のスペースは無視する()
        {
            var result = CommandLineUtils.ParseCommandLineArgs(" this is commandline");
            Assert.AreEqual(result.Length, 3, "パースした配列の要素数");
            Assert.AreEqual(result[0], "this", "パース後の要素");
            Assert.AreEqual(result[1], "is", "パース後の要素");
            Assert.AreEqual(result[2], "commandline", "パース後の要素");
        }

        [TestMethod]
        public void 単一文字列のパース_末尾のスペースは無視する()
        {
            var result = CommandLineUtils.ParseCommandLineArgs("this is commandline ");
            Assert.AreEqual(result.Length, 3, "パースした配列の要素数");
            Assert.AreEqual(result[0], "this", "パース後の要素");
            Assert.AreEqual(result[1], "is", "パース後の要素");
            Assert.AreEqual(result[2], "commandline", "パース後の要素");
        }

        [TestMethod]
        public void 単一文字列のパース_クォート中のスペースは引数に含める()
        {
            var result = CommandLineUtils.ParseCommandLineArgs("this \"is commandline\"");
            Assert.AreEqual(result.Length, 2, "パースした配列の要素数");
            Assert.AreEqual(result[0], "this", "パース後の要素");
            Assert.AreEqual(result[1], "is commandline", "パース後の要素");
        }

        [TestMethod]
        public void 単一文字列のパース_クォートのオンオフはパラメータの区切りにしない()
        {
            var result = CommandLineUtils.ParseCommandLineArgs("this is\" commandline\"");
            Assert.AreEqual(result.Length, 2, "パースした配列の要素数");
            Assert.AreEqual(result[0], "this", "パース後の要素");
            Assert.AreEqual(result[1], "is commandline", "パース後の要素");
        }

        [TestMethod]
        public void 単一文字列のパース_クォートをパラメータに含める()
        {
            var result = CommandLineUtils.ParseCommandLineArgs("this is \\\"commandline\\\"");
            Assert.AreEqual(result.Length, 3, "パースした配列の要素数");
            Assert.AreEqual(result[0], "this", "パース後の要素");
            Assert.AreEqual(result[1], "is", "パース後の要素");
            Assert.AreEqual(result[2], "\"commandline\"", "パース後の要素");
        }

        [TestMethod]
        public void 正常系_引数無し()
        {
            var result = CommandLineUtils.Parse(
                new string[] { },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "input",
                        ShortSwitch = 'i',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 0, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 0, "スイッチ付きパラメータの数");
        }

        [TestMethod]
        public void 正常系_ロングスイッチ()
        {
            var result = CommandLineUtils.Parse(
                new string[] {
                    "--input", "IN01"
                },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "input",
                        ShortSwitch = 'i',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 0, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 1, "スイッチ付きパラメータの数");

            var inputSw = result.Options["input"];
            Assert.AreEqual(inputSw.Count(), 1, "スイッチに指定したパラメータの数");
            Assert.AreEqual(inputSw.ElementAt(0), "IN01", "スイッチに指定したパラメータ");
        }

        [TestMethod]
        public void 正常系_ショートスイッチ()
        {
            var result = CommandLineUtils.Parse(
                new string[] {
                    "-i", "IN01"
                },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "input",
                        ShortSwitch = 'i',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 0, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 1, "スイッチ付きパラメータの数");

            Assert.AreEqual(result.Options.ContainsKey("input"), true, "ロングスイッチに変換");
            var inputSw = result.Options["input"];
            Assert.AreEqual(inputSw.Count(), 1, "スイッチに指定したパラメータの数");
            Assert.AreEqual(inputSw.ElementAt(0), "IN01", "スイッチに指定したパラメータ");
        }

        [TestMethod]
        public void 正常系_ショートスイッチ_ロングスイッチに変換できない()
        {
            var result = CommandLineUtils.Parse(
                new string[] {
                    "-i", "IN01"
                },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = null,
                        ShortSwitch = 'i',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 0, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 1, "スイッチ付きパラメータの数");

            Assert.AreEqual(result.Options.ContainsKey("i"), true, "ロングスイッチに変換");
            var inputSw = result.Options["i"];
            Assert.AreEqual(inputSw.Count(), 1, "スイッチに指定したパラメータの数");
            Assert.AreEqual(inputSw.ElementAt(0), "IN01", "スイッチに指定したパラメータ");
        }

        [TestMethod]
        public void 正常系_同一スイッチに複数パラメータ()
        {
            var result = CommandLineUtils.Parse(
                new string[] {
                    "--input", "IN01",
                    "--output", "OUT01",
                    "--input", "IN02",
                },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "input",
                        ShortSwitch = 'i',
                        AllowMultiple = true,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 0, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 2, "スイッチ付きパラメータの数");

            var inputSw = result.Options["input"];
            Assert.AreEqual(inputSw.Count(), 2, "スイッチに指定したパラメータの数");
            Assert.AreEqual(inputSw.ElementAt(0), "IN01", "スイッチに指定したパラメータ");
            Assert.AreEqual(inputSw.ElementAt(1), "IN02", "スイッチに指定したパラメータ");
        }

        [TestMethod]
        public void 正常系_スイッチ無しパラメータ()
        {
            var result = CommandLineUtils.Parse(
                new string[] {
                    "--input", "IN01",
                    "ARGS1",
                    "--output", "OUT01",
                    "ARGS2",
                },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "input",
                        ShortSwitch = 'i',
                        AllowMultiple = true,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 2, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 2, "スイッチ付きパラメータの数");

            Assert.AreEqual(result.Args.ElementAt(0), "ARGS1", "スイッチ無しパラメータ1");
            Assert.AreEqual(result.Args.ElementAt(1), "ARGS2", "スイッチ無しパラメータ2");

            var inputSw = result.Options["input"];
            Assert.AreEqual(inputSw.ElementAt(0), "IN01", "スイッチに指定したパラメータ");
        }

        [TestMethod]
        public void 正常系_パラメータを要求しないスイッチ()
        {
            var result = CommandLineUtils.Parse(
                new string[] {
                    "--input", "IN01",
                    "--output", "OUT01",
                    "ARGS2",
                },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "input",
                        ShortSwitch = 'i',
                        AllowMultiple = true,
                        RequireParam = false,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 2, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 2, "スイッチ付きパラメータの数");

            var inputSw = result.Options["input"];
            Assert.AreEqual(inputSw.Count(), 1, "スイッチに指定したパラメータの数");
            Assert.AreEqual(inputSw.ElementAt(0), "", "パラメータを要求しないので空文字列を格納している");

            var args = result.Args;
            Assert.AreEqual(args.ElementAt(0), "IN01", "パラメータの位置にあるがスイッチがパラメータを要求していない");
            Assert.AreEqual(args.ElementAt(1), "ARGS2", "通常のパラメータ");
        }


        [TestMethod]
        public void 正常系_スイッチの終端()
        {
            var result = CommandLineUtils.Parse(
                new string[] {
                    "--input", "IN01",
                    "--",
                    "--output", "OUT01",
                    "ARGS2",
                },
                new CommandLineOptionDefinition[]
                {
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "input",
                        ShortSwitch = 'i',
                        AllowMultiple = true,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "output",
                        ShortSwitch = 'o',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                    new CommandLineOptionDefinition()
                    {
                        LongSwitch = "verbose",
                        ShortSwitch = 'v',
                        AllowMultiple = false,
                        RequireParam = true,
                    },
                });

            Assert.AreEqual(result.Args.Count(), 3, "スイッチ無し引数の数");
            Assert.AreEqual(result.Options.Count, 1, "スイッチ付きパラメータの数");

            var inputSw = result.Options["input"];
            Assert.AreEqual(inputSw.Count(), 1, "スイッチに指定したパラメータの数");
            Assert.AreEqual(inputSw.ElementAt(0), "IN01", "スイッチに指定したパラメータ");

            var args = result.Args;
            Assert.AreEqual(args.ElementAt(0), "--output", "終端以降なのでオプションとして解釈されない");
        }

        [TestMethod]
        public void 異常系_パラメータ定義に指定されていないスイッチ()
        {
            try
            {
                var result = CommandLineUtils.Parse(
                    new string[] {
                        "--inputx", "IN01",
                        "--output", "OUT01",
                    },
                    new CommandLineOptionDefinition[]
                    {
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "input",
                            ShortSwitch = 'i',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "output",
                            ShortSwitch = 'o',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "verbose",
                            ShortSwitch = 'v',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                    });

                Assert.Fail("パラメータ必須が無視された");
            }
            catch (InvalidSwitchException)
            { }
            catch (Exception)
            { Assert.Fail("想定外の例外がスローされた"); }
        }

        [TestMethod]
        public void 異常系_パラメータ必須のスイッチにパラメータ漏れ()
        {
            try
            {
                var result = CommandLineUtils.Parse(
                    new string[] {
                        "--input",
                        "--output", "IN02",
                    },
                    new CommandLineOptionDefinition[]
                    {
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "input",
                            ShortSwitch = 'i',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "output",
                            ShortSwitch = 'o',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "verbose",
                            ShortSwitch = 'v',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                    });

                Assert.Fail("パラメータ必須が無視された");
            }
            catch (MissingParameterException)
            { }
            catch (Exception)
            { Assert.Fail("想定外の例外がスローされた"); }
        }

        [TestMethod]
        public void 異常系_同一スイッチの複数回指定不可()
        {
            try
            {
                var result = CommandLineUtils.Parse(
                    new string[] {
                        "--input", "IN01",
                        "--input", "IN02",
                    },
                    new CommandLineOptionDefinition[]
                    {
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "input",
                            ShortSwitch = 'i',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "output",
                            ShortSwitch = 'o',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                        new CommandLineOptionDefinition()
                        {
                            LongSwitch = "verbose",
                            ShortSwitch = 'v',
                            AllowMultiple = false,
                            RequireParam = true,
                        },
                    });

                Assert.Fail("複数回指定不可が無視された");
            }
            catch(MultipleParametersException)
            { }
            catch (Exception)
            { Assert.Fail("想定外の例外がスローされた"); }
        }
    }
}
