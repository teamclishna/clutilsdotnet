﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace clUtils.util
{
    [TestClass]
    public class EncodingUtilsTest
    {
        [TestMethod]
        public void DetectJapanese_SJIS()
        {
            var bytes = LoadFile(@"\Resources\EncodingUtils\shiftjis.txt");
            Assert.AreEqual(EncodingUtils.ENCODING_SJIS, bytes.DetectJapanese(null));
        }

        [TestMethod]
        public void DetectJapanese_EUC()
        {
            var bytes = LoadFile(@"\Resources\EncodingUtils\euc.txt");
            Assert.AreEqual(EncodingUtils.ENCODING_EUC, bytes.DetectJapanese(null));
        }

        [TestMethod]
        public void DetectJapanese_UTF8N()
        {
            var bytes = LoadFile(@"\Resources\EncodingUtils\utf8n.txt");
            Assert.AreEqual(EncodingUtils.ENCODING_UTF8N, bytes.DetectJapanese(null));
        }

        [TestMethod]
        public void DetectBase64_No_Decode()
        {
            var source = "Help EFF this month—no donation necessary!";
            var expect = "Help EFF this month—no donation necessary!";
            Assert.AreEqual(expect, EncodingUtils.Base64ToString(source));
        }

        [TestMethod]
        public void DetectBase64_UTF8()
        {
            var source = "=?UTF-8?B?Q0VBVEVDIDIwMjAgT05MSU5FIOacrOaXpeacgOe1guaXpe+8geOBvuOBoOOBvuOBoOmWk+OBq+WQiOOBhuadpeWgtOeZu+mMsu+8iOeEoeaWme+8ieWPl+S7mOS4re+8gQk=?=";
            var expect = "CEATEC 2020 ONLINE 本日最終日！まだまだ間に合う来場登録（無料）受付中！\t";
            Assert.AreEqual(expect, EncodingUtils.Base64ToString(source));
        }

        [TestMethod]
        public void DetectBase64_ISO2022()
        {
            var source = "=?iso-2022-jp?B?GyRCI0UjVCNDJV4lJCVsITwlOCQrJGkkTiQqQ04kaSQ7GyhC?=";
            var expect = "ＥＴＣマイレージからのお知らせ";
            Assert.AreEqual(expect, EncodingUtils.Base64ToString(source));
        }

        [TestMethod]
        public void DetectBase64_ISO2022_MultiLine()
        {
            var source = "=?ISO-2022-JP?B?GyRCIVozKzpFNFY2YSEqIVsbKEIxMC8yMBskQiFBGyhCMTA=?=  =?ISO-2022-JP?B?LzIzGyRCIVRDTz9ePnBKcyRHJUclOCU/JWsyPTtZGyhC?=  =?ISO-2022-JP?B?GyRCMWchVRsoQkNFQVRFQxskQiUqJXMlaSUkJXMlOyVfGyhC?=  =?ISO-2022-JP?B?GyRCJUohPCROJDQwRkZiIUozdDwwMnE8UiU8JXMlaiVzIUsbKEI=?=";
            var expect = "【開催間近！】10/20～10/23《地図情報でデジタル化支援》CEATECオンラインセミナーのご案内（株式会社ゼンリン）";
            Assert.AreEqual(expect, EncodingUtils.Base64ToString(source));
        }

        [TestMethod]
        public void DetectQP_No_Decode()
        {
            var source = "test string";
            var expect = "test string";
            Assert.AreEqual(expect, EncodingUtils.QPToString(source));
        }

        [TestMethod]
        public void DetectQP_UTF8()
        {
            var source = "=?utf-8?Q?=E3=83=86=E3=82=B9=E3=83=88=E6=96=87=E5=AD=97=E5=88=97?=";
            var expect = "テスト文字列";
            Assert.AreEqual(expect, EncodingUtils.QPToString(source));
        }

        [TestMethod]
        public void DetectQP_SJIS()
        {
            var source = "=?shift_jis?Q?=83=65=83=58=83=67=95=b6=8e=9a=97=f1?=";
            var expect = "テスト文字列";
            Assert.AreEqual(expect, EncodingUtils.QPToString(source));
        }

        [TestMethod]
        public void DetectQP_SJIS_MultiLIne()
        {
            var source = "=?shift_jis?Q?=96=C0=98=66=83=81=81=5B=83=8b=82=c9?=\r\n\t=?shift_jis?Q?=82=E6=82=AD=82=A0=82=E9=?=  =?shift_jis?Q?=83=47=83=93=83=52=81=5B=83=66=83=42=83=93=83=4F?=";
            var expect = "迷惑メールによくあるエンコーディング";
            Assert.AreEqual(expect, EncodingUtils.QPToString(source));
        }

        /// <summary>
        /// テストプロジェクト内の指定したファイルを読み込んでそのバイト列を返す
        /// </summary>
        /// <param name="relativePath"></param>
        /// <returns></returns>
        private byte[] LoadFile(string relativePath)
        {
            var codebaseDir = System.IO.Directory.GetParent(Assembly.GetExecutingAssembly().Location);
            using (var st = new System.IO.FileStream(codebaseDir + relativePath, System.IO.FileMode.Open))
            {
                var bytes = new byte[32768];
                var readSize = st.Read(bytes, 0, 32768);
                return bytes;
            }
        }
    }
}
