﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.xml
{
    /// <summary>
    /// XMLタグ1つの内容を格納するクラス.
    /// </summary>
    public class XmlNode
    {
        /// <summary>
        /// このタグがテキストノードを表している場合、その文字列を格納するフィールド
        /// </summary>
        private string _text;
  
        /// <summary>
        /// このタグのタグ名.
        /// </summary>
        public String Name
        { get; set; }

        /// <summary>
        /// このタグの中に格納されているタグを出現順に格納したリスト
        /// </summary>
        public List<XmlNode> Children
        { get; private set; }

        /// <summary>
        /// このタグに設定されている属性
        /// </summary>
        public Dictionary<string, string> Attributes
        { get; private set; }

        /// <summary>
        /// 文字列ノードの場合、その値.
        /// このノードが通常のタグである場合、このタグ内の文字列ノード部分を連結した値.
        /// </summary>
        public String Text
        {
            get
            {
                if (this.Name == null)
                { return this._text; }
                else
                {
                    var result = new StringBuilder();
                    foreach (var node in this.Children.Where<XmlNode>(n => n.Name == null))
                    { result.Append(node.Text); }
                    return result.ToString();
                }
            }
            set
            { this._text = value; }
        }

        /// <summary>
        /// このタグ、およびこのタグの子ノードからタグを取り除いた文字列
        /// </summary>
        public String InnerText
        {
            get
            {
                var result = new StringBuilder();
                foreach(var node in this.Children)
                {
                    if(node.Name != null)
                    { result.Append(node.InnerText); }
                    else
                    { result.Append(node.Text); }
                }
                return result.ToString();
            }
        }
 
        /// <summary>
        /// このノードに含まれるテキストの整数表現を返す
        /// </summary>
        public int IntValue
        {
            get
            {
                try
                { return int.Parse(this.InnerText); }
                catch (Exception)
                { throw new XmlInvalidTypeException(this, typeof(int)); }
            }
        }

        /// <summary>
        /// このノードに含まれるテキストの実数表現を返す
        /// </summary>
        public float FloatValue
        {
            get
            {
                try
                { return float.Parse(this.InnerText); }
                catch (Exception)
                { throw new XmlInvalidTypeException(this, typeof(float)); }
            }
        }

        /// <summary>
        /// このノードに含まれるテキストの実数表現を返す
        /// </summary>
        public double DoubleValue
        {
            get
            {
                try
                { return double.Parse(this.InnerText); }
                catch (Exception)
                { throw new XmlInvalidTypeException(this, typeof(double)); }
            }
        }

        /// <summary>
        /// このノードに含まれるテキストのbool値での表現を返す.
        /// テキストは ”True” または "False" でなければならない.
        /// </summary>
        public bool BooleanValue
        {
            get
            {
                try
                { return bool.Parse(this.InnerText); }
                catch (Exception)
                { throw new XmlInvalidTypeException(this, typeof(bool)); }
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public XmlNode()
        {
            this.Children = new List<XmlNode>();
            this.Attributes = new Dictionary<string, string>();
            //テキストの初期値として空文字列を設定しておく
            this.Text = string.Empty;
        }

        /// <summary>
        /// このノードを起点として、指定したノード名の階層に該当する最初のノードを返す
        /// </summary>
        /// <param name="keys">検索するノードの階層を格納した配列</param>
        /// <returns>指定したノード階層に存在する最初のノード. 該当するノードが存在しない場合はnull</returns>
        public XmlNode FindNode(string[] keys)
        {
            if (keys.Length == 1)
            { return FindNode(keys[0]); }
            else
            {
                string[] nextKeys = new string[keys.Length - 1];
                Array.Copy(keys, 1, nextKeys, 0, keys.Length - 1);
                foreach (var child in this.Children)
                {
                    if (keys[0] == child.Name)
                    { return child.FindNode(nextKeys); }
                }
                return null;
            }
        }

        /// <summary>
        /// このノードに含まれる指定したノード名の最初のノードを返す
        /// </summary>
        /// <param name="keys">検索するノード名</param>
        /// <returns>指定したノード名の最初のノード. 該当するノードが存在しない場合はnull</returns>
        public XmlNode FindNode(string key)
        {
            foreach (var child in this.Children)
            {
                if (key == child.Name)
                { return child; }
            }
            return null;
        }

        /// <summary>
        /// このノードを起点として、指定したノード名の階層に該当するノードをリストに格納して返す
        /// </summary>
        /// <param name="keys">検索するノードの階層を格納した配列</param>
        /// <returns>指定したノード階層に存在するすべてのノードを格納したList。該当するノードが存在しない場合は空のList</returns>
        public List<XmlNode> FindNodes(string[] keys)
        {
            if (keys.Length == 1)
            { return FindNodes(keys[0]); }
            else
            {
                var result = new List<XmlNode>();
                string[] nextKeys = new string[keys.Length - 1];
                Array.Copy(keys, 1, nextKeys, 0, keys.Length - 1);
                foreach (var child in this.Children)
                {
                    if (keys[0] == child.Name)
                    { result.AddRange(child.FindNodes(nextKeys)); }
                }
                return result;
            }
        }

        /// <summary>
        /// このノードに含まれる指定したノード名のノードをリストに格納して返す
        /// </summary>
        /// <param name="keys">検索するノード名</param>
        /// <returns>指定したノード名のすべてのノードを格納したList。該当するノードが存在しない場合は空のList</returns>
        public List<XmlNode> FindNodes(string key)
        {
            var result = new List<XmlNode>();
            foreach (var child in this.Children)
            {
                if (key == child.Name)
                { result.Add(child); }
            }
            return result;
        }

        public override string ToString()
        {
            //ノードの内容を再帰的に返す
            StringBuilder sb = new StringBuilder();
            sb.Append("{").Append(this.Name != null ? this.Name : null).Append(" ");
            //属性を持っていればその内容
            if (0 < this.Attributes.Count)
            {
                sb.Append("[");
                foreach (var key in this.Attributes.Keys)
                { sb.Append(key).Append("=").Append(this.Attributes[key]).Append(", "); }
                sb.Append("]");
            }
            //子ノードを持っていればその内容
            if (0 < this.Children.Count)
            {
                foreach (var child in this.Children)
                { sb.Append(child.ToString()); }
            }
            //このノードに格納されているテキスト
            sb.Append(this.Text);
            sb.Append("}");

            return sb.ToString();
        }
    }
}
