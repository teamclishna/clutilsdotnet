﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace clUtils.xml
{
    /// <summary>
    /// XMLファイルの入出力時に使うユーティリティ群
    /// </summary>
    public static class XmlUtil
    {
        /// <summary>
        /// 指定したノードに格納されている文字列を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="key">取得したい値の格納されているノード名</param>
        /// <param name="defaultString">指定したノードの値が存在しなかった場合の代替文字列</param>
        /// <returns>指定したノードにある値</returns>
        public static string GetNodeString(this XmlNode node, string key, string defaultString)
        { return GetNodeString(node, new string[] { key }, defaultString); }

        /// <summary>
        /// 指定したノードに格納されている文字列を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="keys">取得したい値の格納されているノード名までのパス</param>
        /// <param name="defaultString">指定したノードの値が存在しなかった場合の代替文字列</param>
        /// <returns>指定したノードにある値</returns>
        public static string GetNodeString(this XmlNode node, string[] keys, string defaultString)
        {
            var findNode = node.FindNode(keys);
            return (findNode != null ? findNode.Text : defaultString);
        }

        /// <summary>
        /// 指定したノードに格納されているbool値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="key">取得したい値の格納されているノード名</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static bool GetNodeBoolean(this XmlNode node, string key, bool defaultValue)
        { return GetNodeBoolean(node, new string[] { key }, defaultValue); }

        /// <summary>
        /// 指定したノードに格納されているbool値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="keys">取得したい値の格納されているノード名までのパス</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static bool GetNodeBoolean(this XmlNode node, string[] keys, bool defaultValue)
        {
            var findNode = node.FindNode(keys);
            return (findNode != null ? findNode.BooleanValue : defaultValue);
        }

        /// <summary>
        /// 指定したノードに格納されている整数値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="key">取得したい値の格納されているノード名</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static int GetNodeInt(this XmlNode node, string key, int defaultValue)
        { return GetNodeInt(node, new string[] { key }, defaultValue); }

        /// <summary>
        /// 指定したノードに格納されている整数値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="keys">取得したい値の格納されているノード名までのパス</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static int GetNodeInt(this XmlNode node, string[] keys, int defaultValue)
        {
            var findNode = node.FindNode(keys);
            return (findNode != null ? findNode.IntValue : defaultValue);
        }

        /// <summary>
        /// 指定したノードに格納されている小数値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="key">取得したい値の格納されているノード名</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static float GetNodeFloat(this XmlNode node, string key, float defaultValue)
        { return GetNodeFloat(node, new string[] { key }, defaultValue); }

        /// <summary>
        /// 指定したノードに格納されている小数値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="keys">取得したい値の格納されているノード名までのパス</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static float GetNodeFloat(this XmlNode node, string[] keys, float defaultValue)
        {
            var findNode = node.FindNode(keys);
            return (findNode != null ? findNode.FloatValue : defaultValue);
        }

        /// <summary>
        /// 指定したノードに格納されている小数値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="key">取得したい値の格納されているノード名</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static double GetNodeDouble(this XmlNode node, string key, double defaultValue)
        { return GetNodeDouble(node, new string[] { key }, defaultValue); }

        /// <summary>
        /// 指定したノードに格納されている小数値を取得して返す
        /// </summary>
        /// <param name="node">検索の起点とするノード</param>
        /// <param name="keys">取得したい値の格納されているノード名までのパス</param>
        /// <param name="defaultValue">指定したノードが存在しなかった場合に返すデフォルト値</param>
        /// <returns>指定したノードにある値</returns>
        public static double GetNodeDouble(this XmlNode node, string[] keys, double defaultValue)
        {
            var findNode = node.FindNode(keys);
            return (findNode != null ? findNode.DoubleValue : defaultValue);
        }
    }
}
