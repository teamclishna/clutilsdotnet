﻿using System.Xml;
using System.IO;

using clUtils.log;

namespace clUtils.xml
{
    /// <summary>
    /// XMLファイルを読み込んで構造体的に格納するためのクラス
    /// </summary>
    public class XmlReader
    {
        public ILogger Logger
        { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public XmlReader()
        {
            this.Logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);
        }

        /// <summary>
        /// 指定したパスにあるファイルからXMLファイルを読み込む
        /// </summary>
        /// <param name="path">XMLファイルのパス</param>
        /// <returns>再帰的に読み込んだノードの構造情報</returns>
        public XmlNode LoadFromFile(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            { return LoadFromStream(fs); }
        }

        /// <summary>
        /// 入力ストリームからXMLファイルを読み込む
        /// </summary>
        /// <param name="input">XMLファイルを示す入力ストリーム</param>
        /// <returns>再帰的に読み込んだノードの構造情報</returns>
        public XmlNode LoadFromStream(Stream input)
        {
            XmlNode rootNode = null;
            var reader = System.Xml.XmlReader.Create(input);
            //最初の開始タグが出現するまで読み進めてそこから再帰的に読み込みを行う
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    rootNode = LoadNode(reader);
                    break;
                }
            }
            return rootNode;
        }

        /// <summary>
        /// 文字列からXMLを生成する
        /// </summary>
        /// <param name="xmlString">XML文字列</param>
        /// <returns>再帰的に読み込んだノードの構造情報</returns>
        public XmlNode LoadFromString(string xmlString)
        {
            XmlNode rootNode = null;
            var reader = System.Xml.XmlReader.Create(new StringReader(xmlString));
            //最初の開始タグが出現するまで読み進めてそこから再帰的に読み込みを行う
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    rootNode = LoadNode(reader);
                    break;
                }
            }
            return rootNode;
        }

        /// <summary>
        /// XMLのノードを再帰的に読み込んでその結果を返す
        /// </summary>
        /// <param name="reader">
        /// XMLリーダ. このメソッドが呼び出された時点で、readerの現在位置は
        /// XmlNodeType.Elementであることが前提である.
        /// </param>
        /// <returns></returns>
        private XmlNode LoadNode(System.Xml.XmlReader reader)
        {
            var result = new XmlNode() { Name = reader.LocalName, };
            Logger.PrintPush(LogLevel.DEBUG, "Load Node \"" + result.Name + "\"");
            
            //このノードの属性を検索する
            if (reader.HasAttributes)
            {
                for (var cnt = 0; cnt < reader.AttributeCount; cnt++)
                {
                    reader.MoveToAttribute(cnt);
                    result.Attributes.Add(reader.LocalName, reader.Value);
                }
            }

            //このノードが空タグである場合はここで返す
            if (reader.IsStartElement() && reader.IsEmptyElement)
            {
                Logger.Print(LogLevel.DEBUG, result.Name + "is empty element.");
                return result;
            }

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        //開始タグが出現したら再帰的に読み込む
                        result.Children.Add(LoadNode(reader));
                        break;

                    case XmlNodeType.Text:
                        //テキストの場合はタグ名無しのノードを格納する
                        result.Children.Add(new XmlNode() { Name = null, Text = reader.Value, });
                        break;

                    case XmlNodeType.CDATA:
                        //CDATAもテキストとして格納しておく(バイナリを格納しているようなXMLを扱うことはないだろうし)
                        result.Children.Add(new XmlNode() { Name = null, Text = reader.Value, });
                        break;

                    case XmlNodeType.EndElement:
                        //タグの終端が現れたらここで終了、生成していたノードオブジェクトを返す
                        Logger.PrintPop();
                        return result;

                    default:
                        break;
                }
            }

            Logger.PrintPop();
            return result;
        }
    }
}
