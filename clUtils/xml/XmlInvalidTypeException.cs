﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.xml
{
    /// <summary>
    /// XMLのノード値の型が想定と異なる場合にスローする例外
    /// </summary>
    public class XmlInvalidTypeException : Exception
    {
        public XmlInvalidTypeException()
            : base()
        { }

        public XmlInvalidTypeException(XmlNode node, Type requireType)
            : base(string.Format(
            "Can\'t pase node text \"{0}\" to type {1}, in Node {2}",
            new object[] { node.Text, requireType.Name, node.Name }))
        { }
    }
}
