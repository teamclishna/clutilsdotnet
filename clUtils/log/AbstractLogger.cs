﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace clUtils.log
{
    /// <summary>
    /// ロガーの抽象クラスで、ログレベルの設定、取得、および局所的ログレベル設定のスタックを実装する
    /// </summary>
    public abstract class AbstractLogger : ILogger
    {
        /// <summary>
        /// スタックに積むログの内容を示す構造体
        /// </summary>
        private struct OutputStackStruct
        {
            public string message;
            public LogLevel level;
        }

        /// <summary>
        /// ログ出力時に先頭に出力する、ログレベルを示す文字列
        /// </summary>
        private static Dictionary<LogLevel, string> _logHeader;

        /// <summary>
        /// スタック形式で出力している文字列の現在のスタックの状態
        /// </summary>
        private Stack<OutputStackStruct> _outputStack;

        public bool ShowCurrentLine
        { get; set; }

        public bool ShowThreadID
        { get; set; }

        public bool ShowTimestamp
        { get; set; }

        /// <summary>
        /// 配列として渡されたログレベルを内部でハッシュセットとして保持するためのフィールド
        /// </summary>
        private HashSet<LogLevel> _globalLogLevel;

        public LogLevel[] OutputLogLevel
        {
            get { return _globalLogLevel.ToArray(); }
            set { this._globalLogLevel = new HashSet<LogLevel>(value); }
        }

        public bool ShowLogLevel
        { get; set; }

        public bool ShowCallStackIndentation
        { get; set; }

        public string IndentString
        { get; set; }

        /// <summary>
        /// ローカルのログレベルを判定するためのスタック
        /// </summary>
        private Stack<HashSet<LogLevel>> _localLogLevelStack;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        static AbstractLogger()
        {
            //ログ出力時の接頭子を生成しておく
            _logHeader = new Dictionary<LogLevel, string>() {
                {LogLevel.ERROR, "[E] "},
                {LogLevel.WARN, "[W] "},
                {LogLevel.INFO, "[I] "},
                {LogLevel.DEBUG, "[D] "},
                {LogLevel.VERBOSE, "[V] "},
            };
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        protected AbstractLogger()
            : base()
        {
            //ログレベルの初期値を「設定なし」として明示的に初期化しておく
            this.OutputLogLevel = new LogLevel[] { };
            this._localLogLevelStack = new Stack<HashSet<LogLevel>>();

            // デフォルトはヘッダ全項目表示、スレッドID、コールスタックのインデント表示は無し
            this.ShowTimestamp = true;
            this.ShowThreadID = false;
            this.ShowCurrentLine = true;
            this.ShowLogLevel = true;
            this.ShowCallStackIndentation = false;
            this.IndentString = "";
            this._outputStack = new Stack<OutputStackStruct>();
        }

        public void PushLocalLogLevel(LogLevel[] newLevel)
        {
            _localLogLevelStack.Push(new HashSet<LogLevel>(newLevel));
        }

        public void PopLocalLogLevel()
        {
            if (_localLogLevelStack.Count > 0)
            { _localLogLevelStack.Pop(); }
        }

        /// <summary>
        /// 指定した出力レベルのログを実際に出力するべきかどうかを判定して返す
        /// </summary>
        /// <param name="outputLevel">出力しようとする出力レベル</param>
        /// <returns>出力が必要であると判定した場合はtrue</returns>
        protected bool CheckOutput(LogLevel outputLevel)
        {
            //スタックが格納されていれば、その最上位の値で判定する
            //スタックが空ならグローバルの値で判定する
            var levelSet = (_localLogLevelStack.Count > 0 ? _localLogLevelStack.Peek() : _globalLogLevel);
            return levelSet.Contains(outputLevel);
        }

        public void Print(
            LogLevel level,
            string s,
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0)
        {
            if (CheckOutput(level))
            { Print(CreateLogHeader(level, sourceFile, methodName, callerLine) + CreateStackString() + s); }
        }

        public void Print(
            LogLevel level,
            string prefix,
            string s,
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0)
        {
            if (CheckOutput(level))
            { Print(CreateLogHeader(level, sourceFile, methodName, callerLine) + CreateStackString() + prefix + s); }
        }

        public void PrintPush(
            LogLevel level,
            string s,
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0)
        {
            if (CheckOutput(level))
            {
                Print(level, LoggerPrefix.StartPrefix, s, sourceFile, methodName, callerLine);
                _outputStack.Push(new OutputStackStruct() { level = level, message = s });
            }
        }

        public void PrintPop(
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0)
        {
            if (0 < _outputStack.Count)
            {
                var s = _outputStack.Pop();
                Print(s.level, LoggerPrefix.EndPrefix, s.message, sourceFile, methodName, callerLine);
            }
        }

        /// <summary>
        /// ログ出力の先頭部分を生成する
        /// </summary>
        /// <returns></returns>
        private string CreateLogHeader(LogLevel level, string sourceFile, string methodName, int line)
        {
            var sb = new StringBuilder();

            if (this.ShowTimestamp)
            { sb.Append(DateTime.Now.ToString("HH:mm:ss.fff")).Append(" "); }

            if(this.ShowThreadID)
            {
#if NETSTANDARD1_4
#else
                sb.Append(string.Format("@{0} ", System.Threading.Thread.CurrentThread.ManagedThreadId));
#endif
            }
            if (this.ShowLogLevel)
            { sb.Append(_logHeader[level]); }

            if (this.ShowCurrentLine)
            { sb.Append(GetCurrentPosition(sourceFile, methodName, line)); }

            return sb.ToString();
        }

        /// <summary>
        /// ログ出力時のインデント文字列を生成する
        /// </summary>
        /// <returns></returns>
        private string CreateStackString()
        {
            if (this.ShowCallStackIndentation)
            { return string.Concat(Enumerable.Repeat(this.IndentString, this._outputStack.Count)); }
            else
            { return ""; }
        }

        /// <summary>
        /// ソースコード上の現在位置を示す文字列を生成して返す.
        /// 呼び出し位置は ShowCurrentLine プロパティをtrueに設定している、かつデバッグビルドの場合にのみ出力する.
        /// </summary>
        /// <returns>ロガーを呼び出したソースコード上の位置を示す文字列表現. スタックトレースを出力する設定で無い場合は空文字列</returns>
        private string GetCurrentPosition(string sourceFile, string methodName, int line)
        {
#if DEBUG
            if (this.ShowCurrentLine)
            { return string.Format("{0}#{1}:{2} ", sourceFile, methodName, line); }
            else
            { return ""; }
#else
            return "";
#endif
        }

        /// <summary>
        /// 出力可否の判定を終えて実際にログを出力するためのメソッド.
        /// 出力先に関する実装を行う
        /// </summary>
        /// <param name="s">出力するログ文字列</param>
        protected abstract void Print(string s);
    }
}
