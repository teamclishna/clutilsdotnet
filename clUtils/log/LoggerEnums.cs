﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.log
{
    /// <summary>
    /// ログの出力レベルを識別するための列挙体
    /// </summary>
    public enum LogLevel
    {
        ERROR,
        WARN,
        INFO,
        DEBUG,
        VERBOSE,
    }

    /// <summary>
    /// ファクトリが生成するロガーを識別するための文字列定数群.
    /// </summary>
    public static class LoggerName
    {
        public const string StdOutLogger = "clUtils.log.StdOutLogger";
        public const string StdErrLogger = "clUtils.log.StdErrLogger";
        public const string ConsoleLogger = "clUtils.log.ConsoleLogger";
        public const string TextWriterLogger = "clUtils.log.TextWriterLogger";
        public const string NullLogger = "clUtils.log.NullLogger";
    }

    /// <summary>
    /// ログ出力時に出力する接頭子の定義(よく使うものだけここで定義)
    /// </summary>
    public static class LoggerPrefix
    {
        public static readonly string StartPrefix = "START:";
        public static readonly string EndPrefix = "END  :";
    }
}
