﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.log
{
    /// <summary>
    /// ログの出力要求を受け取るためのインターフェイス
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// 出力対象とするログのレベルを取得、または設定する.
        /// Printメソッドの引数で指定したログレベルがこの配列中の要素に含まれていれば実際に出力を行う.
        /// </summary>
        LogLevel[] OutputLogLevel { get; set; }

        /// <summary>
        /// ログ出力時の現在時刻の出力可否を設定する
        /// </summary>
        bool ShowTimestamp { get; set; }

        /// <summary>
        /// ログ出力時のスレッドIDの出力可否を設定する
        /// </summary>
        bool ShowThreadID { get; set; }

        /// <summary>
        /// ログ出力時のログレベルの出力可否を設定する
        /// </summary>
        bool ShowLogLevel { get; set; }

        /// <summary>
        /// ログの出力箇所の出力可否を設定する
        /// </summary>
        bool ShowCurrentLine { get; set; }

        /// <summary>
        /// PrintPush, PrintPop を使用するときにコールスタックの深さに応じてインデントを入れるかどうかを設定する
        /// </summary>
        bool ShowCallStackIndentation { get; set; }

        /// <summary>
        /// ShowCallStackIndentation を有効にした場合のインデントに使用する文字列
        /// </summary>
        string IndentString { get; set; }

        /// <summary>
        /// 一時的に出力対象のログレベルを変更する.
        /// </summary>
        /// <param name="newLevel">新しく出力対象として設定するログレベル</param>
        void PushLocalLogLevel(LogLevel[] newLevel);

        /// <summary>
        /// PushLocalLogLevelで設定したログレベルを元に戻す.
        /// スタックされているログレベル設定が存在しない場合は何もしない
        /// </summary>
        void PopLocalLogLevel();

        /// <summary>
        /// ログの出力を行う
        /// </summary>
        /// <param name="level">このログの重要度を示すレベル</param>
        /// <param name="s">出力する文字列</param>
        /// <param name="sourceFile">呼び出し元のソースファイル. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="methodName">呼び出し元のメソッド名. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="callerLine">呼び出し元のソースファイルの行番号. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        void Print(
            LogLevel level,
            string s,
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0);

        /// <summary>
        /// ログの出力を行う
        /// </summary>
        /// <param name="level">このログの重要度を示すレベル</param>
        /// <param name="prefix">出力するメッセージsの前につける識別子</param>
        /// <param name="s">出力する文字列</param>
        /// <param name="sourceFile">呼び出し元のソースファイル. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="methodName">呼び出し元のメソッド名. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="callerLine">呼び出し元のソースファイルの行番号. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        void Print(
            LogLevel level,
            String prefix,
            String s,
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0);

        /// <summary>
        /// スタック形式でログの出力を行う.
        /// このメソッドで指定した文字列sを出力した後にPrintPopメソッドを呼び出すと、再度sを出力する
        /// </summary>
        /// <param name="level">このログの重要度を示すレベル</param>
        /// <param name="s">出力する文字列</param>
        /// <param name="sourceFile">呼び出し元のソースファイル. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="methodName">呼び出し元のメソッド名. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="callerLine">呼び出し元のソースファイルの行番号. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        void PrintPush(
            LogLevel level,
            String s,
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0);

        /// <summary>
        /// スタック形式でログの出力を行う.
        /// 最後に実行した PrintPush メソッドで指定した文字列を再度出力し、スタックを1つ取り除く.
        /// 既にスタックが空である場合は何も出力しない.
        /// </summary>
        /// <param name="sourceFile">呼び出し元のソースファイル. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="methodName">呼び出し元のメソッド名. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        /// <param name="callerLine">呼び出し元のソースファイルの行番号. コンパイラによって自動的に設定するのでコードからの設定不要.</param>
        void PrintPop(
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFile = "",
            [System.Runtime.CompilerServices.CallerMemberName]string methodName = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int callerLine = 0);
    }
}
