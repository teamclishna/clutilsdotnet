﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace clUtils.log
{
    /// <summary>
    /// デバッグコンソールに対するログ出力を行うクラス
    /// </summary>
    public class ConsoleLogger : AbstractLogger
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ConsoleLogger()
            : base()
        { }

        /// <summary>
        /// コンストラクタ.
        /// インスタンスはファクトリからの生成のみ許可する.
        /// </summary>
        /// <param name="s"></param>
        protected override void Print(string s)
        { Debug.WriteLine(s); }
    }
}
