﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.log
{
    /// <summary>
    /// ある2点間の経過時間を計測するストップウォッチ
    /// </summary>
    public class Stopwatch
    {
        private DateTime startTime;
        private DateTime currentLap;
        private DateTime previousLap;

        /// <summary>
        /// 開始時刻
        /// </summary>
        public DateTime StartTime
        { get { return this.startTime; } }

        /// <summary>
        /// 直前にラップタイムを取得した時刻
        /// </summary>
        public DateTime SplitTime
        { get { return this.currentLap; } }

        /// <summary>
        /// 開始時刻から直前に取得した時刻までの経過時間
        /// </summary>
        public TimeSpan TotalTime
        { get { return currentLap - startTime; } }

        /// <summary>
        /// 前のラップ取得時刻から直前のラップ取得時刻までの経過時間
        /// </summary>
        public TimeSpan LapTime
        { get { return currentLap - previousLap; } }

        /// <summary>
        /// 計測を開始する
        /// </summary>
        /// <returns></returns>
        public void Start()
        {
            startTime = DateTime.Now;
            currentLap = startTime;
            previousLap = startTime;
        }

        /// <summary>
        /// ラップライムを取得する
        /// </summary>
        /// <returns></returns>
        public TimeSpan Lap()
        {
            previousLap = currentLap;
            currentLap = DateTime.Now;
            return LapTime;
        }

        public Stopwatch()
        {
            //各時刻をインスタンスの生成時刻にしておく
            startTime = DateTime.Now;
            currentLap = startTime;
            previousLap = startTime;
        }
    }
}
