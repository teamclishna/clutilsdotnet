﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.log
{
    /// <summary>
    /// ロガーオブジェクトを生成するためのクラス
    /// </summary>
    public static class LoggerFactory
    {
        /// <summary>
        /// ロガーオブジェクトのインスタンスを生成して返す
        /// </summary>
        /// <param name="loggerName">ロガーオブジェクト名</param>
        /// <returns>生成したロガーオブジェクト</returns>
        public static ILogger CreateLogger(string loggerName)
        {
            try
            {
                var loggerType = Type.GetType(loggerName);
                var logger = Activator.CreateInstance(loggerType) as ILogger;
                if (logger == null)
                { throw new ArgumentException("Invalid logger name: \"" + loggerName + "\"."); }

                return logger;
            }
            catch (Exception eUnknown)
            { throw new ArgumentException("Invalid logger name: \"" + loggerName + "\".", eUnknown); }
        }

        /// <summary>
        /// ロガーオブジェクトのインスタンスを生成して返す
        /// </summary>
        /// <param name="loggerName">ロガーオブジェクト名</param>
        /// <param name="level">出力対象とするレベルの初期値</param>
        /// <returns>生成したロガーオブジェクト</returns>
        public static ILogger CreateLogger(string loggerName, LogLevel[] level)
        {
            var logger = CreateLogger(loggerName);
            logger.OutputLogLevel = level;
            return logger;
        }
    }
}
