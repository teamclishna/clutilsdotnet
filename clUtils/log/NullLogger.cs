﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace clUtils.log
{
    /// <summary>
    /// どこにもログを出力しないクラス.
    /// Loggerをプロパティとして持っている場合に、とりあえずダミーのインスタンスを設定しておきたい場合に使う.
    /// </summary>
    public class NullLogger : ILogger
    {
        public LogLevel[] OutputLogLevel
        { get; set; }

        public bool ShowCurrentLine
        { get; set; }

        public bool ShowTimestamp
        { get; set; }

        public bool ShowThreadID
        { get; set; }

        public bool ShowLogLevel
        { get; set; }

        public bool ShowCallStackIndentation
        { get; set; }

        public string IndentString
        { get; set; }

        public void PushLocalLogLevel(LogLevel[] newLevel)
        { }

        public void PopLocalLogLevel()
        { }

        public void Print(LogLevel level, string s, [CallerFilePath] string sourceFile = "", [CallerMemberName] string methodName = "", [CallerLineNumber] int callerLine = 0)
        { }

        public void Print(LogLevel level, string prefix, string s, [CallerFilePath] string sourceFile = "", [CallerMemberName] string methodName = "", [CallerLineNumber] int callerLine = 0)
        { }

        public void PrintPush(LogLevel level, string s, [CallerFilePath] string sourceFile = "", [CallerMemberName] string methodName = "", [CallerLineNumber] int callerLine = 0)
        { }

        public void PrintPop([CallerFilePath] string sourceFile = "", [CallerMemberName] string methodName = "", [CallerLineNumber] int callerLine = 0)
        { }
    }
}
