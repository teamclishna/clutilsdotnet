﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace clUtils.log
{
    /// <summary>
    /// 指定した出力ストリームに対するログ出力を行うクラス.
    /// このクラス自身ではストリームのクローズを行わないので、呼び出し元で責任をもって閉じる必要がある
    /// </summary>
    public class TextWriterLogger : AbstractLogger
    {
        public TextWriter Writer { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TextWriterLogger()
            : base()
        { }

        /// <summary>
        /// ログ出力.
        /// </summary>
        /// <param name="s">出力する文字列</param>
        protected override void Print(string s)
        { Writer.WriteLine(s); }
    }
}
