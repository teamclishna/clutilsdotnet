﻿using System;
using System.Collections.Generic;
using System.Text;

namespace clUtils.log
{
    /// <summary>
    /// 標準エラー出力に対して出力を行うロガー.
    /// ログというよりはアプリケーション内の出力処理に使う
    /// </summary>
    public class StdErrLogger : AbstractLogger
    {
        protected override void Print(string s)
        { Console.Error.WriteLine(s); }
    }
}
