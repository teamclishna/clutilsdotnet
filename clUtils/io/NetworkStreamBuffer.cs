﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using clUtils.log;

namespace clUtils.io
{

    /// <summary>
    /// ストリームからバイト列を読み込むときに使うバッファ
    /// </summary>
    public class NetworkStreamBuffer
    {
        /// <summary>
        /// ストリームから読み込んだバイト列
        /// </summary>
        private byte[] buffer;

        /// <summary>
        /// バッファサイズ
        /// </summary>
        public int BufferSize
        { get { return buffer.Length; } }

        /// <summary>
        /// バッファに格納されているデータのサイズ
        /// </summary>
        public int DataSize
        { get; private set; }

        /// <summary>
        /// 現在バッファに格納されているデータのコピーを取得する.
        /// ここで取得するバイト列のサイズはバッファイサイズそのままではなく、有効なデータの長さだけ返す.
        /// </summary>
        public byte[] Data
        {
            get
            {
                byte[] result = new byte[this.DataSize];
                Array.Copy(buffer, result, this.DataSize);
                return result;
            }
        }

        /// <summary>
        /// 読み込んだ内容をデバッグ出力するためのロガー
        /// </summary>
        public ILogger Logger
        { get; set; }

        /// <summary>
        /// バイト列の先頭から指定したバイト数だけデータを削除し、それ以降の内容を先頭に移動させる
        /// </summary>
        /// <param name="length"></param>
        public void Shrink(int length)
        {
            Logger.Print(LogLevel.DEBUG, string.Format("Shrink {0} bytes.", new object[] { length }));

            if (length == 0)
            { return; }

            byte[] newBuffer = new byte[this.BufferSize];
            if (length < this.DataSize)
            {
                Array.Copy(this.buffer, length, newBuffer, 0, this.DataSize - length);
                this.DataSize -= length;
            }
            else
            {
                //有効なデータサイズより大きい値を削除するよう指定した場合、
                //データは空の状態にする
                this.DataSize = 0;
            }
            Logger.Print(LogLevel.DEBUG, string.Format("data size is {0} bytes.", new object[] { this.DataSize }));
            this.buffer = newBuffer;
        }

        /// <summary>
        /// 指定したストリームからデータを読み込み、このインスタンスのバッファに保持する
        /// バッファがいっぱいになったら読み込みを中止する.
        /// </summary>
        /// <param name="inputStream"></param>
        /// <returns>ストリームから読み込んだバイト数</returns>
        public int ReadFrom(Stream inputStream)
        {
            int readSize = inputStream.Read(this.buffer, this.DataSize, this.BufferSize - this.DataSize);
            Logger.Print(LogLevel.DEBUG, string.Format("Read {0} bytes.", new object[] { readSize }));
            this.DataSize += readSize;
            return readSize;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="bufferSize">バッファサイズ</param>
        public NetworkStreamBuffer(int bufferSize)
        {
            this.buffer = new byte[bufferSize];
            this.DataSize = 0;
            this.Logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);
        }

    }
}
