﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace clUtils.io
{
    /// <summary>
    /// ネットワークストリームに対する文字列の入出力を行うためのユーティリティ関数群
    /// </summary>
    public static class NetworkStreamUtil
    {
        private static readonly int CODEPAGE_ISO2022JP = 50220;

        private static Encoding ISO2022JjpEncoding = null;

        /// <summary>
        /// 改行が出現するまで文字列を読み込み、その内容を返す.
        /// </summary>
        /// <param name="stream">データを入力するストリーム</param>
        /// <param name="buffer">
        /// 読み込んだ内容を格納するバッファ. 取得しようとする文字列の後にデータが続いている可能性がある場合、
        /// 継続する処理でここで渡したインスタンスを再度渡すこと.
        /// </param>
        /// <returns>受信した文字列. ただし、末尾の改行は含まない</returns>
        public static string ReceiveSingleLineString(NetworkStream stream, ref NetworkStreamBuffer buffer)
        {
            // バッファ内に行が残っていればそれを返す
            var lineInBuffer = GetLineFromBuffer(ref buffer);
            if (lineInBuffer != null)
            { return lineInBuffer; }

            //データを読み込む.
            if (0 < buffer.ReadFrom(stream))
            {
                //読み込んだデータの先頭から順に改行コードを探す
                var readData = buffer.Data;
                int index = 0;
                var crlf = false;
                while (index < readData.Length)
                {
                    if (readData[index] == '\n')
                    { break; }
                    else if (readData[index] == '\r')
                    {
                        //\rの場合は、次のバイトが\nかどうかを判定する
                        if (index < readData.Length && readData[index + 1] == '\n')
                        { crlf = true; }
                        break;
                    }
                    index++;
                }

                //先頭が改行であった場合、空文字列を返す
                //そうでない場合は改行の直前までのバイト列を文字列に変換する
                var enc = GetISO2022Encoding();
                var result = (index == 0 ? "" : enc.GetString(readData, 0, index));

                //バッファから改行までを切り取る
                buffer.Shrink(index + (crlf ? 2 : 1));
                return result;
            }
            else
            {
                //データを読み込めなかった場合は空文字列を返す
                return "";
            }
        }

        /// <summary>
        /// バッファ内に残っているデータが改行を含む場合、そこまでのデータを取り出して返す
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private static string GetLineFromBuffer(ref NetworkStreamBuffer buffer)
        {
            //読み込んだデータの先頭から順に改行コードを探す
            var readData = buffer.Data;
            int index = 0;
            var crlf = false;
            while (index < readData.Length)
            {
                if (readData[index] == '\n')
                { break; }
                else if (readData[index] == '\r')
                {
                    //\rの場合は、次のバイトが\nかどうかを判定する
                    if (index < readData.Length && readData[index + 1] == '\n')
                    { crlf = true; }
                    break;
                }
                index++;
            }

            // 終端まで走査しても改行が出現しなかった場合はnullを返して終了
            if(index == readData.Length)
            { return null; }

            //先頭が改行であった場合、空文字列を返す
            //そうでない場合は改行の直前までのバイト列を文字列に変換する
            var enc = GetISO2022Encoding();
            var result = (index == 0 ? "" : enc.GetString(readData, 0, index));

            //バッファから改行までを切り取る
            buffer.Shrink(index + (crlf ? 2 : 1));
            return result;
        }

        /// <summary>
        /// 指定したデリミタ文字列が出現するまで文字列を読み込み、その結果を返す
        /// </summary>
        /// <param name="stream">データを入力するストリーム</param>
        /// <param name="buffer">
        /// 読み込んだ内容を格納するバッファ. 取得しようとする文字列の後にデータが続いている可能性がある場合、
        /// 継続する処理でここで渡したインスタンスを再度渡すこと.
        /// </param>
        /// <param name="delimiter">終端を示すデリミタ文字列</param>
        /// <returns>読み込んだ文字列. 終端のデリミタ文字も含めて返す.</returns>
        public static string ReceiveMultiLineString(
            NetworkStream stream, 
            ref NetworkStreamBuffer buffer, 
            string delimiter)
        {
            //読み込んだ文字列を格納するバッファ
            //とりあえず初期値は10KBくらいにしておく
            var sb = new StringBuilder(10240);
            var enc = Encoding.GetEncoding(50220);

            //データを読み込む
            while (0 < buffer.ReadFrom(stream))
            {
                sb.Append(enc.GetString(buffer.Data, 0, buffer.DataSize));
                buffer.Shrink(buffer.DataSize);

                if (sb.ToString().EndsWith(delimiter))
                { return sb.ToString(); }
            }
            return sb.ToString();
        }

        /// <summary>
        /// ストリームにASCII文字列を送信する
        /// POP3のコマンド見たく、日本語が含まれないことが確実な場合にのみ使用可能
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="msg"></param>
        public static void SendAsciiString(NetworkStream stream, string msg)
        {
            //文字列をバイト列に変換
            var enc = Encoding.ASCII;
            var buf = enc.GetBytes(msg);

            //送信
            stream.Write(buf, 0, buf.Length);
        }

        /// <summary>
        /// メールの受信に使用する iso2022-jp エンコーディングオブジェクトを取得する
        /// </summary>
        /// <returns></returns>
        private static Encoding GetISO2022Encoding()
        {
            if (ISO2022JjpEncoding == null)
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                ISO2022JjpEncoding = Encoding.GetEncoding(CODEPAGE_ISO2022JP);
            }
            return ISO2022JjpEncoding;
        }

    }
}
