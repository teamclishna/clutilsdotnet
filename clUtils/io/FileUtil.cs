﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.io
{
    /// <summary>
    /// ファイルシステムの操作に関するユーティリティ群
    /// </summary>
    public class FileUtil
    {
        /// <summary>
        /// 指定した相対パスを絶対パスに変換する
        /// </summary>
        /// <param name="relPath">絶対パスに変換したい相対パス</param>
        /// <param name="basePath">相対パスの基準とするファイルのフルパス. ディレクトリではなく、基準のディレクトリ上にあるファイルを示す必要がある.</param>
        /// <returns>basePathのあるディレクトリを基準として、relPathが示す場所の絶対パス</returns>
        public static string GetAbsolutePath(string relPath, string basePath)
        {
            var uBase = new Uri(basePath);
            var uTarget = new Uri(uBase, relPath);

            var absoluteUri = uTarget.LocalPath;
            var absolutePathString = Uri.UnescapeDataString(absoluteUri).Replace('/', System.IO.Path.PathSeparator);

            return absolutePathString;
        }

        /// <summary>
        /// 相対パスを生成して返す
        /// </summary>
        /// <param name="basePath">相対パスの基準にするパス. ディレクトリではなく、基準のディレクトリ上にあるファイルを示す必要がある.</param>
        /// <param name="targetPath">相対パスを求める対象になるパス</param>
        /// <returns>basePathで示したパスのディレクトリから見た、targetPathの相対パス</returns>
        public static string GetRelativePath(string basePath, string targetPath)
        {
            var u1 = new Uri(basePath);
            var u2 = new Uri(targetPath);

            //相対URLを取得し、エスケープを解除する
            var relativeUri = u1.MakeRelativeUri(u2).ToString();
            var relativePathString = Uri.UnescapeDataString(relativeUri).Replace('/', System.IO.Path.PathSeparator);

            return relativePathString;
        }

    }
}
