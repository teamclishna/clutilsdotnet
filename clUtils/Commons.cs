﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils
{
    /// <summary>
    /// 特定用途に限定されないユーティリティ関数群
    /// </summary>
    public static class Commons
    {
        /// <summary>
        /// リソースのURIを絶対パスで生成して返す
        /// </summary>
        /// <param name="AssemblyName">リソースが格納されているアセンブリ名</param>
        /// <param name="ResourcePath">リソースのパス</param>
        /// <returns>指定したパスを絶対パスで示すURI</returns>
        public static Uri GetAbsoluteResourceUri(String AssemblyName, String ResourcePath)
        { return new Uri("pack://application:,,,/" + AssemblyName + ";component" + ResourcePath, UriKind.Absolute); }

    }
}
