﻿using System;
using System.Windows.Input;

namespace clUtils.gui
{
    /// <summary>
    /// XAML でボタンなどにバインディングする ICommand の実装.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields
        private Func<object, bool> _canExecute;
        private Action<object> _execute;
        #endregion

        public event EventHandler CanExecuteChanged;

        #region Properties

        /// <summary>
        /// このコマンドが実行可能かどうかの判定処理
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            if (_canExecute != null)
            { return _canExecute(parameter); }
            else
            { return true; }
        }

        /// <summary>
        /// CanExecuteの判定結果が変わる場合に外部から呼び出すイベント
        /// </summary>
        public void RaiseCanExecuteChanged()
        { this.CanExecuteChanged?.Invoke(this, EventArgs.Empty); }

        /// <summary>
        /// このコマンドにによって実行する処理.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        { this._execute?.Invoke(parameter); }

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="canExecute">コマンド実行可否の判定処理の実装. null を指定した場合は常に実行可能となる.</param>
        /// <param name="exec">このコマンドによる処理の実装.</param>
        public RelayCommand(Func<object, bool> canExecute, Action<object> exec)
        {
            this._canExecute = canExecute;
            this._execute = exec;
        }
    }
}
