﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace clUtils.gui
{
    /// <summary>
    /// INotifyPropertyChanged 用のイベントを実装済みの ViewModel
    /// </summary>
    public class NotifiableViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var d = PropertyChanged;
            if (d != null)
            { d(this, new PropertyChangedEventArgs(propertyName)); }
        }
        #endregion
    }
}
