﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.util
{
    /// <summary>
    /// 日本語テキストの判別とデコード、エンコードを行うユーティリティクラス.
    /// 判別可能なエンコーディングは SJIS, EUC で判別に失敗した場合は utf-8 とする。
    /// </summary>
    public static class EncodingUtils
    {
        #region for bytes to string

        /// <summary>エンコーディング名 (Shift-JIS)</summary>
        public static readonly string ENCODING_NAME_SJIS = "shift_jis";
        /// <summary>エンコーディング名 (EUC)</summary>
        public static readonly string ENCODING_NAME_EUC = "euc-jp";
        /// <summary>エンコーディング名 (utf-8 BOMなし、ハイフン表記)</summary>
        public static readonly string ENCODING_NAME_UTF8N = "utf-8n";
        /// <summary>エンコーディング名 (utf-8 BOMなし、アンダーバー表記)</summary>
        public static readonly string ENCODING_NAME_UTF8N2 = "utf_8n";

        public static readonly Encoding ENCODING_SJIS = Encoding.GetEncoding(ENCODING_NAME_SJIS);

        public static readonly Encoding ENCODING_EUC = Encoding.GetEncoding(ENCODING_NAME_EUC);

        public static readonly Encoding ENCODING_UTF8N = new UTF8Encoding(false);

        /// <summary>
        /// 指定したエンコーディング名でバイト列を文字列に変換する
        /// </summary>
        /// <param name="src">変換対象のバイト列</param>
        /// <param name="encodingName">エンコーディング名</param>
        /// <returns>エンコード後の文字列</returns>
        /// <exception cref="ArgumentException">エンコーディングの判別に失敗した場合</exception>
        public static string Decode(this byte[] src, string encodingName)
        { return Encoding.GetEncoding(encodingName).GetString(src); }

        /// <summary>
        /// 指定したバイト列をエンコーディング自動判別して文字列に変換する
        /// </summary>
        /// <param name="src">変換対象のバイト列</param>
        /// <param name="defaultEncoding">判別できなかった場合に適用するデフォルトのエンコーディング</param>
        /// <returns>エンコード後の文字列</returns>
        public static string Decode(this byte[] src, Encoding defaultEncoding)
        { return src.DetectJapanese(defaultEncoding).GetString(src); }

        /// <summary>
        /// 指定したバイト列をエンコーディング自動判別して文字列に変換する
        /// </summary>
        /// <param name="src">変換対象のバイト列</param>
        /// <returns>エンコード後の文字列</returns>
        /// <exception cref="ArgumentException">エンコーディングの判別に失敗した場合</exception>
        public static string Decode(this byte[] src)
        { return src.DetectJapanese(null).GetString(src); }

        /// <summary>
        /// 指定したバイト列の文字コードを自動判別する
        /// </summary>
        /// <param name="src">チェック対象の文字列を格納したバイト列</param>
        /// <param name="defaultEncoding">判別できなかった場合に適用するデフォルトのエンコーディング</param>
        /// <returns>
        /// 判別結果.
        /// 判別できなかった場合は defaultEncoding で指定したエンコーディング.
        /// defaultEncoding に null を指定した場合は ArgumentException をスローする.
        /// </returns>
        public static Encoding DetectJapanese(this byte[] src, Encoding defaultEncoding)
        {
            var index = 0;
            while (index < src.Length)
            {
                var lead = (int)src[index];

                if (0x80 <= lead && lead <= 0x9f)
                {
                    // [0x80, 0x9f] が現れたら SJIS
                    return ENCODING_SJIS;
                }
                else if (0xa1 <= lead && lead <= 0xbf)
                {
                    index++;
                    var trail = (int)src[index];

                    // [0xa1, 0xbf], [0xa1, 0xfe] の場合は EUC
                    if (0xa1 <= trail && trail <= 0xfe)
                    { return ENCODING_EUC; }
                }
                else if (0xc0 <= lead && lead <= 0xdf)
                {
                    index++;
                    var trail = (int)src[index];

                    // [0xc0, 0xdf], [0x80, 0x9f] の場合は utf-8
                    // [0xc0, 0xdf], [0xa0, 0xff] の場合は EUC
                    if (0x80 <= trail && trail <= 0x9f)
                    { return ENCODING_UTF8N; }

                    if (0xa0 <= trail && trail <= 0xff)
                    { return ENCODING_EUC; }
                }
                else if (0xe0 <= lead && lead <= 0xef)
                {
                    index++;
                    var trail = (int)src[index];

                    // [0xe0, 0xef], [0x80, 0xbf] の場合は utf-8
                    // [0xe0, 0xef], [0x40, 0xff] の場合は SJIS
                    if (0x80 <= trail && trail <= 0xbf)
                    { return ENCODING_UTF8N; }

                    if (0x40 <= trail && trail <= 0xff)
                    { return ENCODING_SJIS; }
                }
                else if (0xf0 <= lead && lead <= 0xfe)
                {
                    index++;
                    var trail = (int)src[index];

                    // [0xf0, 0xfe], [0xc0, 0xfe] の場合は EUC
                    if (0xc0 <= trail && trail <= 0xfe)
                    { return ENCODING_EUC; }
                }
                index++;
            }

            // 確定しなかった場合はデフォルトのエンコーディングを返す
            if (defaultEncoding != null)
            { return defaultEncoding; }
            else
            { throw new ArgumentException("Failed detect encoding."); }
        }

        #endregion

        #region for Mail Header decoding

        /// <summary>
        /// ヘッダの日本語部分を識別するためのBASE64エンコードの先頭部分.
        /// {0}部分にエンコーディング名を順にあてはめて、最初に出現したものを使う
        /// </summary>
        private static readonly string BASE64_START = "=?{0}?b?";

        /// <summary>
        /// ヘッダの日本語部分を識別するためのQuoted-Printableエンコードの先頭部分.
        /// {0}部分にエンコーディング名を順にあてはめて、最初に出現したものを使う
        /// </summary>
        private static readonly string QUATED_PRINTABLE_START = "=?{0}?q?";

        /// <summary>
        /// ヘッダの日本語部分を識別するためのBASE64エンコードの終端部分
        /// </summary>
        private static readonly string BASE64_END = "?=";

        /// <summary>
        /// ヘッダのエンコーディングとして使えるエンコーディング名の配列
        /// </summary>
        private static readonly string[] HEADER_ENCODINGS =
        {
            "utf-8",
            "iso-2022-jp",
            "shift_jis",
            "shiftjis",
        };

        /// <summary>
        /// 指定した文字列の中からエンコーディング指定の部分を検索し、指定されているエンコーディング名を返す
        /// </summary>
        /// <param name="src">検索対象の文字列</param>
        /// <param name="encodingHeader">
        /// エンコーディング開始位置の書式を示す文字列。
        /// この文字列に Strinf.Format メソッドでエンコーディング部分を当てはめて検索する
        /// </param>
        /// <param name="startIndex">src中の検索開始位置のインデックス</param>
        /// <returns>見つかったエンコーディング名。見つからなかった場合はnull</returns>
        private static string FindEncoding(
            string src,
            string encodingHeader,
            int startIndex)
        {
            foreach (var encodingName in HEADER_ENCODINGS)
            {
                var startHeader = string.Format(encodingHeader, encodingName);
                if (0 <= src.IndexOf(startHeader.Substring(startIndex), StringComparison.CurrentCultureIgnoreCase))
                { return encodingName; }
            }
            return null;
        }

        /// <summary>
        /// BASE64エンコードされた文字列をデコードする
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string Base64ToString(this string src)
        {
            var sb = new StringBuilder();
            var offset = 0;

            //エンコード名の指定が存在しない場合はそのままの文字列を返す
            var encodeName = FindEncoding(src, BASE64_START, 0);
            if (encodeName == null)
            { return src; }

            var startStirng = string.Format(BASE64_START, encodeName);

            while (offset < src.Length)
            {
                var encodedStartIndex = src.IndexOf(startStirng, offset, StringComparison.CurrentCultureIgnoreCase);
                if (0 <= encodedStartIndex)
                {
                    // BASE64の開始位置までは普通に格納する
                    // ただし、先頭以外 (エンコーディングと次のエンコーディングの間)、かつ部分文字列が空白 (0x20)、TAB (0x09)、または改行 (0x0a, 0x0c) しかなければ
                    // その空白はすべて飛ばす
                    var betweenNext = src.Substring(offset, encodedStartIndex - offset);
                    if (0 < offset && betweenNext.IsSpaceOnly())
                    { }
                    else
                    { sb.Append(betweenNext); }
                    offset = encodedStartIndex + startStirng.Length;

                    // デコード対象の文字列を取得
                    var encodedString = src.Substring(
                        offset,
                        src.IndexOf(BASE64_END, offset, StringComparison.CurrentCultureIgnoreCase) - offset);
                    offset += encodedString.Length + BASE64_END.Length;

                    try
                    {
                        // BASE64エンコーディング部分をデコードする
                        sb.Append(Encoding
                            .GetEncoding(encodeName)
                            .GetString(System.Convert.FromBase64String(encodedString)));
                    }
                    catch (Exception)
                    {
                        // デコードに失敗した場合はそのままの文字列を格納する
                        sb.Append(encodedString);
                    }
                }
                else
                {
                    // 残りの文字列全体を格納する
                    sb.Append(src.Substring(offset));
                    offset = src.Length;
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Quoted-Printableエンコードされたヘッダ文字列をエンコードする
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string QPToString(this string src)
        {
            var sb = new StringBuilder();
            var offset = 0;

            //エンコード名の指定が存在しない場合はそのままの文字列を返す
            var encodeName = FindEncoding(src, QUATED_PRINTABLE_START, 0);
            if (encodeName == null)
            { return src; }

            var startStirng = string.Format(QUATED_PRINTABLE_START, encodeName);
            var encodedBytes = new List<byte>();

            while (offset < src.Length)
            {
                var encodedStartIndex = src.IndexOf(startStirng, offset, StringComparison.CurrentCultureIgnoreCase);
                if (0 <= encodedStartIndex)
                {
                    // ヘッダ位置まで格納
                    // ただし、先頭以外 (エンコーディングと次のエンコーディングの間)、かつ部分文字列が空白 (0x20)、TAB (0x09)、または改行 (0x0a, 0x0c) しかなければ
                    // その空白はすべて飛ばす
                    var betweenNext = src.Substring(offset, encodedStartIndex - offset);
                    if (0 < offset && betweenNext.IsSpaceOnly())
                    { }
                    else
                    { sb.Append(betweenNext); }
                    offset = encodedStartIndex + startStirng.Length;
                    encodedBytes.Clear();

                    // 次の3文字が「=xx」なら16進表記とみなしてバイト列に追加
                    // 異なる規則の記号が現れたところでエンコード済みの文字列が終わったことにする
                    var b = IsQuoted(src, offset);
                    while (0 <= b)
                    {
                        encodedBytes.Add((byte)b);
                        offset += 3;
                        b = IsQuoted(src, offset);
                    }
                    if (0 <= src.IndexOf(BASE64_END, offset))
                    { offset += 2; }

                    // ここまでに格納したバイト列をデコードする
                    if (0 < encodedBytes.Count())
                    {
                        sb.Append(Encoding.GetEncoding(encodeName).GetString(encodedBytes.ToArray()));
                    }
                }
                else
                {
                    // 残りの文字列全体を格納する
                    sb.Append(src.Substring(offset));
                    offset = src.Length;
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// 指定した文字列の指定したオフセットから3文字が Quoted-Printable の文字であるかどうかを判定し、
        /// エンコードされた文字列であれば元のバイト値を返す
        /// </summary>
        /// <param name="src">デコード対象の文字列</param>
        /// <param name="offset">チェック開始位置</param>
        /// <returns>
        /// 該当の文字がエンコードされた文字列であれば、元の値を示す値.
        /// 該当の文字ではない場合は負の値
        /// </returns>
        private static int IsQuoted(string src, int offset)
        {
            if (src.Length < offset + 3)
            { return -1; }

            if (src[offset + 0] != '=')
            { return -1; }

            if (!(src[offset + 1].IsHexDigit() && src[offset + 2].IsHexDigit()))
            { return -1; }

            return Convert.ToUInt16(src.Substring(offset + 1, 2), 16);
        }

        /// <summary>
        /// 指定した文字列が空白 (0x20、0x09、および 0x0a、0x0c) のみであるかどうかを判定して返す
        /// </summary>
        /// <param name="str">判定対象の文字列</param>
        /// <returns>指定した文字列が空白のみの文字列であればtrue</returns>
        private static bool IsSpaceOnly(this string str)
        {
            foreach(var c in str)
            {
                if(c == 0x20 || c == 0x09 || c == 0x0a || c == 0x0c)
                { return true; }
            }
            return false;
        }

        /// <summary>
        /// 指定した文字が16進文字 (0-9, a-f, A-F) のいずれかであるかを判定して返す
        /// </summary>
        /// <param name="c">判定対象の文字</param>
        /// <returns>指定した文字が16進文字であればtrue</returns>
        private static bool IsHexDigit(this char c)
        { return (('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F')); }

        #endregion
    }
}
