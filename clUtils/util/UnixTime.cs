﻿using System;

namespace clUtils.util
{
    /// <summary>
    /// UNIX時間と.netのDateTime型の相互変換を行うユーティリティ関数群
    /// </summary>
    public static class UnixTime
    {
        /// <summary>
        /// UNIX時間の基準にする時刻
        /// </summary>
        private static readonly DateTime UnixTimeOrigin = new DateTime(1970, 1, 1, 0, 0, 0);

        /// <summary>
        /// UNIX時間を示すlong値からDateTime型の値を生成して返す
        /// </summary>
        /// <param name="unixTime">1970/01/01 00:00:00 からの通算秒数</param>
        /// <returns>unixTimeに相当するDateTime型の値</returns>
        public static DateTime FromUnixTime(long unixTime)
        { return UnixTimeOrigin.AddSeconds(unixTime); }

        /// <summary>
        /// 指定したDateTime型の時刻をUNIX時間に変換して返す
        /// </summary>
        /// <param name="dt">変換対象の時刻</param>
        /// <returns>指定した時刻の 1970/01/01 00:00:00 からの通算秒数</returns>
        public static long ToUnixTime(this DateTime dt)
        { return (long)dt.Subtract(UnixTimeOrigin).TotalSeconds; }
    }
}
