﻿using System;
using System.Collections.Generic;
using System.Text;

namespace clUtils.util
{
    /// <summary>
    /// 複数回指定不可のパラメータが複数回指定された場合にスローされる例外
    /// </summary>
    public class MultipleParametersException : Exception
    {
        /// <summary>エラーの原因となったコマンドラインオプション</summary>
        public CommandLineOptionDefinition Option
        { get; set; }

        public MultipleParametersException() : base()
        { }

        public MultipleParametersException(CommandLineOptionDefinition option) : this()
        {
            this.Option = option;
        }
    }
}
