﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.util
{
    /// <summary>
    /// コマンドライン引数から展開したオプションスイッチと引数の一覧
    /// </summary>
    public class CommandLineOptions
    {
        /// <summary>コマンドラインに渡された「素」の引数.</summary>
        public string[] Plain { get; set; }

        /// <summary>
        /// オプションスイッチをキーとし、その引数を値とした辞書.
        /// 長いスイッチ、短いスイッチの両方が定義されている場合は長いスイッチをキーとする.
        /// また、パラメータを指定していないスイッチはnullを値とする.
        /// </summary>
        public Dictionary<string, IEnumerable<string>> Options { get; set; }

        /// <summary>コマンドライン引数からスイッチとそのパラメータを除いた引数のリスト.</summary>
        public IEnumerable<string> Args { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("{").Append("\n")
                .Append("\t{\n");
            foreach (var k in this.Options.Keys)
            {
                sb.Append("\t\t").Append(k);
                if (this.Options[k] != null)
                {
                    sb.Append(" = { ");
                    foreach (var val in this.Options[k])
                    { sb.Append("\"").Append(val).Append("\", "); }
                    sb.Append("}\n");
                }
                else
                { sb.Append("\n"); }
            }
            sb.Append("\t}\n");
            sb.Append("\t{\n");
            foreach (var a in this.Args)
            {
                sb.Append("\t\t\"").Append(a).Append("\"\n");
            }
            sb.Append("\t}\n");
            sb.Append("}\n");

            return sb.ToString();
        }
    }

    /// <summary>
    /// サポートするコマンドラインオプションの定義
    /// </summary>
    public class CommandLineOptionDefinition
    {
        /// <summary>ハイフン1個で指定する短い形式のスイッチ</summary>
        public char ShortSwitch { get; set; }

        /// <summary>ハイフン2個で指定する長い形式のスイッチ</summary>
        public string LongSwitch { get; set; }

        /// <summary>このスイッチがパラメータを要求するかどうか</summary>
        public bool RequireParam { get; set; }

        /// <summary>このスイッチの複数回指定を許可するかどうか</summary>
        public bool AllowMultiple { get; set; }
    }

    /// <summary>
    /// コマンドライン引数のパーサーに関するクラス
    /// </summary>
    public static class CommandLineUtils
    {
        /// <summary>
        /// 単一の文字列として存在するコマンドライン引数をコマンドラインの配列に変換する
        /// </summary>
        /// <remarks>
        /// スペースを区切り記号として配列に変換する.
        /// ダブルクォートで囲まれた範囲内、またはバッククォート直後のスペースは区切り記号として解釈しない.
        /// ダブルクォート自体を引数にする場合は直前にバッククォートを置く.
        /// </remarks>
        /// <param name="cmdLine">コマンドライン文字列</param>
        /// <returns>コマンドラインから変換した配列.</returns>
        public static string[] ParseCommandLineArgs(string cmdLine)
        {
            var result = new List<string>();
            var sb = new StringBuilder();

            var inQuote = false;
            var escaped = false;

            foreach (var c in cmdLine.ToCharArray())
            {
                if (escaped)
                {
                    sb.Append(c);
                    escaped = false;
                }
                else
                {
                    switch (c)
                    {
                        case ' ':
                            // クォート中なら格納する
                            if (inQuote)
                            { sb.Append(c); }
                            else
                            {
                                // 現在格納中のパラメータがあればこれをリストに追加
                                if (0 < sb.Length)
                                {
                                    result.Add(sb.ToString());
                                    sb.Clear();
                                }
                                else
                                { }
                            }
                            break;

                        case '\\':
                            // 次の文字をエスケープ
                            escaped = true;
                            break;

                        case '\"':
                            inQuote = !inQuote;
                            break;

                        default:
                            sb.Append(c);
                            break;
                    }
                }
            }

            // 格納中のパラメータが残っていればここでリストに追加する
            if (0 < sb.Length)
            { result.Add(sb.ToString()); }

            return result.ToArray();
        }
    
        /// <summary>
        /// コマンドライン引数を解析し、その結果をオブジェクトに格納して返す.
        /// </summary>
        /// <remarks>
        /// 解析の規則は以下の通り.
        /// <list type="bullet">
        /// <item><description>引数のスイッチはハイフン1個、またはハイフン2個で始まる.</description></item>
        /// <item><description>同じ意味のショートスイッチ、ロングスイッチの両方を定義している場合、ロングスイッチを指定したものとして格納する.</description></item>
        /// <item><description>スイッチの次にスイッチではない引数がある場合、それは直前のスイッチのパラメータである.</description></item>
        /// <item><description>ただし、パラメータを取らないスイッチの場合、次の引数がパラメータでなくとも格納しない.</description></item>
        /// <item><description>ハイフン1個のスイッチは複数個続けて書くことができる. (ex: a, b, c => -abc)</description></item>
        /// <item><description>ハイフン1個のスイッチを複数個続けて書いた場合、そのスイッチ群はパラメータを取れない.</description></item>
        /// <item><description>同じスイッチが複数回出現した場合、各スイッチのパラメータを出現順にListに格納する.</description></item>
        /// <item><description>ハイフン2個だけ(スイッチ名なし)が出現した場合、それ以降はすべて引数とする.</description></item>
        /// </list>
        /// </remarks>
        /// <param name="args">mainメソッドから渡されたコマンドライン引数</param>
        /// <param name="definitions">アプリケーションが対応するオプションスイッチの定義</param>
        /// <param name="ignoreNotDefinedParameter">オプションスイッチ定義にないオプションを指定していた場合、それをエラーとせずに無視するならtrue</param>
        /// <returns>解析結果</returns>
        public static CommandLineOptions Parse(
            string[] args,
            IEnumerable<CommandLineOptionDefinition> definitions)
        { return Parse(args, definitions, false); }

        /// <summary>
        /// コマンドライン引数を解析し、その結果をオブジェクトに格納して返す.
        /// </summary>
        /// <remarks>
        /// 解析の規則は以下の通り.
        /// <list type="bullet">
        /// <item><description>引数のスイッチはハイフン1個、またはハイフン2個で始まる.</description></item>
        /// <item><description>同じ意味のショートスイッチ、ロングスイッチの両方を定義している場合、ロングスイッチを指定したものとして格納する.</description></item>
        /// <item><description>スイッチの次にスイッチではない引数がある場合、それは直前のスイッチのパラメータである.</description></item>
        /// <item><description>ただし、パラメータを取らないスイッチの場合、次の引数がパラメータでなくとも格納しない.</description></item>
        /// <item><description>ハイフン1個のスイッチは複数個続けて書くことができる. (ex: a, b, c => -abc)</description></item>
        /// <item><description>ハイフン1個のスイッチを複数個続けて書いた場合、そのスイッチ群はパラメータを取れない.</description></item>
        /// <item><description>同じスイッチが複数回出現した場合、各スイッチのパラメータを出現順にListに格納する.</description></item>
        /// <item><description>ハイフン2個だけ(スイッチ名なし)が出現した場合、それ以降はすべて引数とする.</description></item>
        /// </list>
        /// </remarks>
        /// <param name="args">mainメソッドから渡されたコマンドライン引数</param>
        /// <param name="definitions">アプリケーションが対応するオプションスイッチの定義</param>
        /// <param name="ignoreNotDefinedParameter">オプションスイッチ定義にないオプションを指定していた場合、それをエラーとせずに無視するならtrue</param>
        /// <returns>解析結果</returns>
        public static CommandLineOptions Parse(
            string[] args,
            IEnumerable<CommandLineOptionDefinition> definitions,
            bool ignoreNotDefinedParameter)
        {
            // Step 1:
            // スイッチとパラメータを解析して格納する
            var parseResult = ParseArgs(args, definitions);

            // Step 2:
            // パラメータ指定漏れのチェック
            CheckMissingParameters(
                parseResult.switches,
                definitions,
                ignoreNotDefinedParameter);

            return new CommandLineOptions()
            {
                Plain = args,
                Args = parseResult.argsList,
                Options = parseResult.switches,
            };
        }

        /// <summary>
        /// コマンドライン引数を解析し、スイッチとそのパラメータ、スイッチなしの引数の配列に分割する.
        /// </summary>
        /// <param name="args">コマンドライン引数の配列</param>
        /// <param name="definitions">オプションスイッチの定義</param>
        /// <returns>解析結果</returns>
        private static (Dictionary<string, IEnumerable<string>> switches, List<string> argsList) ParseArgs(
            string[] args,
            IEnumerable<CommandLineOptionDefinition> definitions)
        {
            // 名前付きオプションスイッチとそのパラメータ値
            var switches = new Dictionary<string, IEnumerable<string>>();
            // 名前なしのコマンドライン引数
            var argsList = new List<string>();
            // スイッチの終端記号が出現したか?
            var endSwitch = false;

            var index = 0;
            while (index < args.Length)
            {
                var arg = args[index];
                if (endSwitch)
                {
                    // スイッチの終端記号出現後はすべて引数とする
                    argsList.Add(arg);
                }
                else
                {
                    if (arg == "--")
                    {
                        // ハイフン2つだけのスイッチが出現したらそこでスイッチは終わり
                        endSwitch = true;
                    }
                    else if (arg.StartsWith("--"))
                    {
                        // 長いスイッチが指定された場合
                        var switchName = arg.Substring(2);
                        var definition = definitions.FirstOrDefault(x => (x.LongSwitch == switchName));
                        if (definition != null)
                        {
                            if(definition.RequireParam)
                            {
                                // パラメータを要求する場合は次の引数を取得する
                                var addParam = (args.HasParameter(index) ? args[index + 1] : "");
                                switches.AddParameter(switchName, addParam);

                                if (args.HasParameter(index))
                                { index++; }
                            }
                            else
                            {
                                // パラメータを要求しない場合は空のパラメータを格納する
                                switches.AddParameter(switchName, "");
                            }
                        }
                        else
                        {
                            // スイッチに該当するオプション定義が存在しない場合、
                            // とりあえず規則通りに格納する
                            var addParam = (args.HasParameter(index) ? args[index + 1] : "");
                            switches.AddParameter(switchName, addParam);

                            if (args.HasParameter(index))
                            { index++; }
                        }
                    }
                    else if (arg.StartsWith("-"))
                    {
                        // ハイフン1個で始まるスイッチは複数のスイッチを同時に指定できる
                        // 指定されたスイッチが1個だけの場合はパラメータの設定を確認する
                        var currentSwitches = arg.Substring(1).ToCharArray();
                        if (1 < currentSwitches.Length)
                        {
                            foreach (var c in currentSwitches)
                            {
                                // ロングスイッチが存在する場合はこれを取得する
                                var def = definitions.FirstOrDefault(x => x.ShortSwitch == c);
                                var switchName = (def != null ? def.LongSwitch : new string(currentSwitches[0], 1));
                                if(switchName == null)
                                { switchName = new string(currentSwitches[0], 1); }

                                switches.AddParameter(switchName, "");
                            }
                        }
                        else
                        {
                            var def = definitions.FirstOrDefault(x => x.ShortSwitch == currentSwitches[0]);
                            var switchName = (def != null ? def.LongSwitch : new string(currentSwitches[0], 1));
                            if (switchName == null)
                            { switchName = new string(currentSwitches[0], 1); }

                            // このスイッチがパラメータを要求する場合、次の引数を取得する
                            if (def.RequireParam)
                            {
                                if (args.HasParameter(index))
                                {
                                    switches.AddParameter(switchName, args[index + 1]);
                                    index++;
                                }
                                else
                                { switches.AddParameter(switchName, ""); }
                            }
                            else
                            {
                                // パラメータを要求しない場合は空のパラメータを格納する
                                switches.AddParameter(switchName, ""); 
                            }
                        }
                    }
                    else
                    {
                        //ハイフン始まりではない引数は別リストに格納
                        argsList.Add(arg);
                    }
                }
                index++;
            }

            return (switches: switches, argsList: argsList);
        }

        /// <summary>
        /// 解析結果のスイッチからパラメータの指定漏れをチェックする
        /// </summary>
        /// <param name="switches">コマンドライン引数から解析したパラメータ群</param>
        /// <param name="definitions">コマンドラインオプションの定義</param>
        /// <param name="ignoreNotDefinedParameter">オプション定義に存在しないスイッチが指定された場合にエラーではなく無視するならtrue</param>
        private static void CheckMissingParameters(
            Dictionary<string, IEnumerable<string>> switches,
            IEnumerable<CommandLineOptionDefinition> definitions,
            bool ignoreNotDefinedParameter)
        {
            foreach (var key in switches.Keys)
            {
                var sw = definitions.FirstOrDefault(x => key == x.LongSwitch || key == new string(new char[] { x.ShortSwitch }));

                // スイッチ定義にないスイッチを定義している?
                if (sw == null)
                {
                    if (ignoreNotDefinedParameter)
                    { continue; }
                    else
                    { throw new InvalidSwitchException(key); }
                }

                // パラメータ必須のスイッチにパラメータが指定されていない?
                if (sw.RequireParam)
                {
                    // パラメータ未指定 = 空文字のパラメータが存在した場合はエラー
                    if (switches[key].Contains(""))
                    { throw new MissingParameterException(sw); }
                }

                // 複数回指定不可のスイッチに複数回のスイッチが指定されている?
                if (!sw.AllowMultiple)
                {
                    if (1 < switches[key].Count())
                    { throw new MultipleParametersException(sw); }
                }
            }
        }

        /// <summary>
        /// 指定した引数リストの指定したインデックスの次の要素に
        /// パラメータ (ハイフンで始まらない引数) が定義されているかどうかを確認する.
        /// </summary>
        /// <param name="args">引数リスト</param>
        /// <param name="currentIndex">チェックしている引数のインデックス</param>
        /// <returns>インデックスの次の要素にハイフンで始まらないパラメータが指定されていればtrue</returns>
        private static bool HasParameter(this string[] args, int currentIndex)
        {
            return ((currentIndex < args.Length - 1) && !(args[currentIndex + 1].StartsWith("-")));
        }

        /// <summary>
        /// Dictionaryに指定したパラメータを追加する.
        /// Dictionaryに指定したパラメータが存在しない場合は新規にListを追加する.
        /// パラメータが存在済みの場合は登録されているListに値を追加する
        /// </summary>
        /// <param name="dic">パラメータを登録する辞書</param>
        /// <param name="parameterName">追加するパラメータ名</param>
        /// <param name="value">追加するパラメータ</param>
        private static void AddParameter(this Dictionary<string,IEnumerable<string >> dic, string parameterName, string value)
        {
            if (!dic.ContainsKey(parameterName))
            { dic.Add(parameterName, new List<string>()); }

            (dic[parameterName] as List<string>).Add(value);
        }

    }
}
