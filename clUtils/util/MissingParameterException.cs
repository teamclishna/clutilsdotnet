﻿using System;
using System.Collections.Generic;
using System.Text;

namespace clUtils.util
{
    /// <summary>
    /// パラメータ指定が必須のスイッチでパラメータが指定されていなかった場合にスローする例外
    /// </summary>
    public class MissingParameterException : Exception
    {
        /// <summary>エラーの原因となったコマンドラインオプション</summary>
        public CommandLineOptionDefinition Option
        { get; set; }

        public MissingParameterException() : base()
        { }

        public MissingParameterException(CommandLineOptionDefinition option) : this()
        {
            this.Option = option;
        }
    }
}
