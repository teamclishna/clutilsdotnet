﻿using System;
using System.Collections.Generic;
using System.Text;

namespace clUtils.util
{
    /// <summary>
    /// スイッチ定義に含まれないコマンドラインオプションが指定されていた場合にスローする例外
    /// </summary>
    public class InvalidSwitchException : Exception
    {
        /// <summary>エラーの原因となったオプションスイッチ名</summary>
        public string SwitchName
        { get; set; }

        public InvalidSwitchException() : base()
        { }

        public InvalidSwitchException(string name) : this()
        {
            this.SwitchName = name;
        }
    }
}
