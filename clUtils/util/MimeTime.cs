﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace clUtils.util
{
    /// <summary>
    /// ミケネコ時間と.netのDateTime型の相互変換を行うユーティリティ関数群
    /// http://web.archive.org/web/20031217111224/http://www.mikeneko.ne.jp/~lab/misc/miketime/
    /// </summary>
    public static class MimeTime
    {
        private static readonly char[] TIMETABLE =
        {
            '0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F','G','H','I','J',
            'K','L','M','N','P','Q','R','S','T','U',
            'V','W','X','Y','Z','a','b','c','d','e',
            'f','g','h','i','j','k','m','n','o','p',
            'q','r','s','t','u','v','w','x','y','z',
        };

        /// <summary>
        /// ミケネコ時間を示す文字列からDateTime型の値を生成して返す.
        /// </summary>
        /// <param name="mt">ミケネコ時間</param>
        /// <returns></returns>
        public static DateTime FromMikeTime(string mt)
        {
            try
            {
                if (string.IsNullOrEmpty(mt) || 8 < mt.Length)
                { throw new Exception(); }

                var ca = mt.ToArray();
                var y100 = Char2Int(ca[0]);
                var y10 = Char2Int(ca[1]);
                var y1 = Char2Int(ca[2]);
                var m = Char2Int(ca[3]);
                var d = Char2Int(ca[4]);
                var hh = Char2Int(ca[5]);
                var mm = Char2Int(ca[6]);
                var ss = Char2Int(ca[7]);

                if ((y100 < 0) || (y10 < 0) || (y1 < 0) || (m < 0) || (d < 0) || (hh < 0) || (mm < 0) || (ss < 0))
                { throw new Exception(); }

                return new DateTime((y100 * 100) + (y10 * 10) + y1, m, d, hh, mm, ss);
            }
            catch (Exception)
            {
                throw new ArgumentException(
                    string.Format(
                        "invalid MikeTime value: {0}",
                        (mt != null ? string.Format("\"{0}\"", mt) : "(null)")
                        ));
            }
        }

        /// <summary>
        /// DateTime型の値からミケネコ時間を示す文字列を生成して返す.
        /// </summary>
        /// <param name="dt">変換対象の時刻</param>
        /// <returns>指定した時刻を示すミケネコ時間を表す文字列</returns>
        public static string ToMikeTime(this DateTime dt)
        {
            return new string(new char[] {
                TIMETABLE[(int)( dt.Year/100)],
                TIMETABLE[(int)((dt.Year % 100)/10)],
                TIMETABLE[dt.Year % 10],
                TIMETABLE[dt.Month],
                TIMETABLE[dt.Day],
                TIMETABLE[dt.Hour],
                TIMETABLE[dt.Minute],
                TIMETABLE[dt.Second],
            });
        }

        private static int Char2Int(char c)
        { return Array.IndexOf(TIMETABLE, c); }
    
        private static char Int2Char(int n)
        { return TIMETABLE[n]; }
    }
}
