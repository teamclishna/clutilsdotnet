﻿using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace clUtils.gui
{
    /// <summary>
    /// ハードウェアキーの「戻る」で前画面に戻る機能を実装するためのPage
    /// </summary>
    public class BackButtonSupportedPage : Page
    {
        public BackButtonSupportedPage()
            : base()
        {
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            //「戻る」に対応するためのイベントを解除する
            Windows.UI.Core.SystemNavigationManager.GetForCurrentView().BackRequested -= OnBackPressed;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //「戻る」に対応するためのイベントを登録する
            Windows.UI.Core.SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                Frame.CanGoBack ? Windows.UI.Core.AppViewBackButtonVisibility.Visible :
                Windows.UI.Core.AppViewBackButtonVisibility.Collapsed;
            Windows.UI.Core.SystemNavigationManager.GetForCurrentView().BackRequested += OnBackPressed;
        }

        /// <summary>
        /// ハードウェアキーの「戻る」ボタンを押された時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="backArgs"></param>
        public void OnBackPressed(object sender, BackRequestedEventArgs backArgs)
        {
            //前の画面に戻る
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
                backArgs.Handled = true;
            }
        }
    }
}
