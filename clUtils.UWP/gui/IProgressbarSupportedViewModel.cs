﻿using System.ComponentModel;

namespace clUtils.gui
{
    /// <summary>
    /// 処理中のプログレスバー表示を ViewModel でサポートするためのインターフェイス
    /// </summary>
    public interface IProgressbarSupportedViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// プログレスバーの表示属性.
        /// このプロパティをプログレスバー、およびそのラベルの Visibility プロパティにバインディングすること.
        /// </summary>
        Windows.UI.Xaml.Visibility ProgressbarVisibility { get; set; }

        /// <summary>
        /// 画面上部のプログレスバーに表示するメッセージ.
        /// プログレスバーを非表示にする場合は null を設定する.
        /// このプロパティをプログレスバーのラベルとして表示する TextArea にバインディングすること.
        /// </summary>
        string ProgressbarMessage { get; set; }

        /// <summary>
        /// プログレスバーの進捗状況.
        /// 進捗状況が計算できない場合 (最初の情報取得中など) は true を設定する.
        /// このプロパティをプログレスバーの IsIntermediate プロパティにバインディングすること.
        /// </summary>
        bool IsIntermediate { get; set; }

        /// <summary>
        /// プログレスバーの現在の進捗値.
        /// [0.0, 1.0] の範囲で指定する.
        /// このプロパティをプログレスバーの Value プロパティにバインディングすること.
        /// </summary>
        double ProgressbarValue { get; set; }
    }
}
