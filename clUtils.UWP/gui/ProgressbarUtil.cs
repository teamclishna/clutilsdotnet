﻿using System;
using Windows.Foundation.Metadata;
using Windows.UI.ViewManagement;

using clUtils.system;

namespace clUtils.gui
{
    /// <summary>
    /// IProgressbarSupportedViewModel で画面にプログレスバーを表示するためのユーティリティクラス
    /// </summary>
    public static class ProgressbarUtil
    {
        /// <summary>
        /// デスクトップ用ステータスバー(タイトルバー)の型定義
        /// </summary>
        private static readonly string TYPE_STATUSBAR_DESKTOP = "Windows.UI.ViewManagement.ApplicationView";
        /// <summary>
        /// モバイル用ステータスバーの型定義
        /// </summary>
        private static readonly string TYPE_STATUSBAR_MOBILE = "Windows.UI.ViewManagement.StatusBar";

        /// <summary>
        /// 現在の実行環境のデバイスファミリ
        /// </summary>
        public static DeviceFamily CurrentDeviceFamily { get; private set; }

        /// <summary>
        /// static コンストラクタ
        /// </summary>
        static ProgressbarUtil()
        {
            var dev = Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily;
            switch (dev)
            {
                case "Windows.Desktop":
                    CurrentDeviceFamily = DeviceFamily.Desktop;
                    break;
                case "Windows.Mobile":
                    CurrentDeviceFamily = DeviceFamily.Mobile;
                    break;
                default:
                    CurrentDeviceFamily = DeviceFamily.Unknown;
                    break;
            }
        }

        /// <summary>
        /// プログレスバーの値を更新する.
        /// </summary>
        /// <param name="vm">更新対象のプログレスバーにバインディングしている ViewModel</param>
        /// <param name="message">プログレスバーに表示するメッセージ文字列. null に設定した場合、プログレスバーを非表示にする.</param>
        /// <param name="showProgress">進行状況を明示的に示す場合は true、進行状況が明示できない場合(点が流れていくだけの表示)は false</param>
        /// <param name="progressValue">showProgressにtrueを設定した場合、進行状況を [0.0, 1.0] の範囲で指定する</param>
        public static async void UpdateProgressbar(
            this IProgressbarSupportedViewModel vm,
            string message,
            bool showProgress,
            double progressValue)
        {
            if (ApiInformation.IsTypePresent(TYPE_STATUSBAR_DESKTOP))
            {
                //デスクトップの場合はViewModelのプロパティを更新する
                vm.ProgressbarVisibility = (message != null ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed);
                vm.ProgressbarMessage = message;
                vm.IsIntermediate = showProgress;
                vm.ProgressbarValue = progressValue;
            }
            if (ApiInformation.IsTypePresent(TYPE_STATUSBAR_MOBILE))
            {
                //モバイルの場合はAPI経由で通知領域にプログレスバーを表示／非表示する
                var statusBar = StatusBar.GetForCurrentView();
                if (statusBar != null)
                {
                    if (message != null)
                    {
                        var indicator = statusBar.ProgressIndicator;
                        indicator.Text = message;
                        indicator.ProgressValue = (showProgress ? (double?)progressValue : null);

                        await statusBar.ShowAsync();
                        await indicator.ShowAsync();
                    }
                    else
                    {
                        await statusBar.HideAsync();
                    }
                }
            }
        }
    }
}
