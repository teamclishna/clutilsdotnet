﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace clUtils.xml
{
    /// <summary>
    /// XMLファイルを読み込んで構造体的に格納するためのクラス.
    /// UWP用にメソッドを追加している.
    /// </summary>
    public class XmlReaderUWP :clUtils.xml.XmlReader
    {
        /// <summary>
        /// アプリケーションのインストールフォルダに格納しているXMLファイルを読み込む
        /// </summary>
        /// <param name="resourceName">アプリケーションのインストールフォルダを起点とした相対パスで表現したリソースのパス</param>
        /// <returns>再帰的に読み込んだノードの構造情報</returns>
        public async Task<XmlNode> LoadFromAppInstalledFolder(string resourceName)
        {
            //インストールパスを取得
            var appInstalledPath = Windows.ApplicationModel.Package.Current.InstalledLocation;
            
            //対象のファイルに対する入力ストリームを開く
            //以降の処理は非同期で行う
            var targetFile = await appInstalledPath.GetFileAsync(resourceName);
            using (var stream = await targetFile.OpenStreamForReadAsync())
            { return LoadFromStream(stream); }
        }
    }
}
