﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace clUtils.util
{
    /// <summary>
    /// ネット関係のユーティリティクラス
    /// </summary>
    public static class WebUtils
    {
        /// <summary>
        /// 指定したURLからコンテンツを取得してバイト列で返す
        /// </summary>
        /// <param name="url">コンテンツを取得するURL</param>
        /// <returns>取得したコンテンツのバイト列</returns>
        public static async Task<byte[]> GetBytesFromURL(string url)
        {
            var req = WebRequest.Create(url);
            using (var responce = await req.GetResponseAsync())
            using (var stream = responce.GetResponseStream())
            {
                var bytes = new byte[responce.ContentLength];
                var readPosition = 0;
                var readSize = 0;
                while (0 < (readSize = await stream.ReadAsync(
                    bytes,
                    readPosition,
                    (int)responce.ContentLength - readPosition)))
                { readPosition += readSize; }

                return bytes;
            }
        }

        /// <summary>
        /// バイト列から画像を生成して返す
        /// </summary>
        /// <param name="bytes">画像のもとになるバイト列</param>
        /// <returns>生成した画像</returns>
        public static async Task<BitmapImage> CreateImage(byte[] bytes)
        {
            //画像のバイト列をいったん全部読み込み、これをランダムアクセス可能なMemoryStreamにしてから画像のソースにする
            var memStream = new MemoryStream(bytes);
            var img = new BitmapImage();
            await img.SetSourceAsync(memStream.AsRandomAccessStream());
            return img;
        }
    }
}
