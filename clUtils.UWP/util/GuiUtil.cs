﻿using System.Text;
using Windows.Foundation.Metadata;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace clUtils.util
{
    /// <summary>
    /// GUIの実装のためのユーティリティクラス
    /// </summary>
    public static class GuiUtil
    {
        private static readonly string TYPENAME_KEYBOARD_ACCELARATOR = "Windows.UI.Xaml.Input.KeyboardAccelerator";
        private static readonly string TYPENAME_APPBARBUTTON = "Windows.UI.Xaml.Controls.AppBarButton";
        private static readonly string PROPERTYNAME_TEXTOVERRIDE = "KeyboardAcceleratorTextOverride";

        /// <summary>
        /// 指定したUI要素に対してキーボードアクセラレータを追加する
        /// </summary>
        /// <param name="target">アクセラレータの設定対象のコントロール</param>
        /// <param name="key">アクセラレータキー</param>
        /// <param name="modifiers">アクセラレータキーと同時に押下するモディファイアキー</param>
        public static void AddKeyboardAccelarator(
            this UIElement target,
            Windows.System.VirtualKey key,
            Windows.System.VirtualKeyModifiers modifiers)
        {
            // キーボードアクセラレータが実装されているかどうか
            if (ApiInformation.IsTypePresent(TYPENAME_KEYBOARD_ACCELARATOR))
            {
                target.AccessKey = key.ToString();
                target.KeyboardAccelerators.Add(new KeyboardAccelerator()
                {
                    Key = key,
                    Modifiers = modifiers,
                });

                var appBtn = target as AppBarButton;
                if (appBtn != null)
                { appBtn.SetKeyboardAcceleratorText(key, modifiers); }
            }
        }

        /// <summary>
        /// メニュー項目に対してキーボードアクセラレータのラベルを設定する
        /// </summary>
        /// <param name="appBtn">アクセラレータの設定対象のメニュー項目</param>
        /// <param name="key">アクセラレータキー</param>
        /// <param name="modifiers">アクセラレータキーと同時に押下するモディファイアキー</param>
        public static void SetKeyboardAcceleratorText(
            this AppBarButton appBtn,
            Windows.System.VirtualKey key,
            Windows.System.VirtualKeyModifiers modifiers)
        {
            // キーボードアクセラレータのラベル表示が実装されているかどうか
            if (ApiInformation.IsPropertyPresent(TYPENAME_APPBARBUTTON, PROPERTYNAME_TEXTOVERRIDE))
            {
                var sb = new StringBuilder();
                if (modifiers != Windows.System.VirtualKeyModifiers.None)
                { sb.Append(modifiers.ToString()).Append("+"); }
                sb.Append(key.ToString());

                appBtn.KeyboardAcceleratorTextOverride = sb.ToString();
            }
        }
    }
}
