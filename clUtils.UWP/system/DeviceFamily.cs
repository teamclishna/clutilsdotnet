﻿namespace clUtils.system
{
    /// <summary>
    /// 実行環境のデバイスファミリを示す列挙子.
    /// 現在はPC、モバイル、それ以外のみ判定する
    /// </summary>
    public enum DeviceFamily
    {
        /// <summary>不明</summary>
        Unknown = 0,
        /// <summary>デスクトップ</summary>
        Desktop = 1,
        /// <summary>モバイル</summary>
        Mobile = 2,
    }
}
