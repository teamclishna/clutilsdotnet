﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clUtils.gui
{
    /// <summary>
    /// ユーザコントロールに設定するインターフェイス
    /// </summary>
    public interface IChild
    {
        /// <summary>
        /// このユーザーウィンドウを配置する親ウィンドウ.
        /// 外部からは設定のみ可能
        /// </summary>
        IParentWindow ParentWindow
        { set; }
    }
}
