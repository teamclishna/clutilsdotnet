﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace clUtils.gui
{
    /// <summary>
    /// Win32←→WPF間のスケーリングを行うためのクラス
    /// </summary>
    public static class Scaling
    {
        /// <summary>
        /// WPFの論理座標とWin32の物理座標の間で相互変換を行う
        /// </summary>
        /// <param name="visual">スケーリングの対象とするWPFコントロール</param>
        /// <param name="sourcePos">変換元の座標系</param>
        /// <param name="toWin32">WPF -&gt; Win32への変換を行う場合はtrue. Win32 -&gt; WPFへの変換を行う場合はfalse</param>
        /// <returns></returns>
        public static System.Drawing.Point GetScaledPoint(
            Visual visual, 
            System.Drawing.Point sourcePos, 
            bool toWin32)
        {
            var source = PresentationSource.FromVisual(visual);
            var scale = (source != null && source.CompositionTarget != null
                ? new System.Drawing.PointF(
                    (float)source.CompositionTarget.TransformToDevice.M11,
                    (float)source.CompositionTarget.TransformToDevice.M22)
                : new System.Drawing.PointF(1.0f, 1.0f));

            if (toWin32)
            { return new System.Drawing.Point((int)(sourcePos.X * scale.X), (int)(sourcePos.Y * scale.Y)); }
            else
            { return new System.Drawing.Point((int)(sourcePos.X / scale.X), (int)(sourcePos.Y / scale.Y)); }
        }
    }
}
