﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace clUtils.gui
{
    /// <summary>
    /// 背景を透過する、BeOS風のタイトルバーを持つウィンドウの基底クラス
    /// </summary>
    public partial class TransparentWindow : Window, IParentWindow
    {
        #region Properties

        private bool _visibleTitleBar = true;

        /// <summary>タイトルバーを表示するかどうか</summary>
        public bool VisibleTitleBar
        {
            get { return this._visibleTitleBar; }
            set
            {
                
                this._visibleTitleBar = value;
                this.labelWindowCaption.Visibility = (value ? Visibility.Visible : Visibility.Collapsed);
            }
        }

        private bool _visibleCloseButton = true;

        /// <summary>タイトルバー表示時にクローズボタンを表示するかどうか</summary>
        public bool VisibleCloseButton
        {
            get { return this._visibleCloseButton; }
            set
            {
                _visibleCloseButton = value;
                this.btnWindowCloseButton.Visibility = (this._visibleTitleBar 
                    ? (value ? Visibility.Visible : Visibility.Collapsed) 
                    : Visibility.Collapsed);
            }
        }

        private bool _resizable = true;
        /// <summary>ウィンドウのリサイズが可能かどうか</summary>
        public bool Resizable
        {
            get { return this._resizable; }
            set
            {
                this._resizable = value;
                //最大化、最小化ボタン
                var visible = (this._visibleTitleBar
                    ? (value ? Visibility.Visible : Visibility.Collapsed)
                    : Visibility.Collapsed);
                this.btnWindowMaximizeButton.Visibility = visible;
                this.btnWindowMinimizeButton.Visibility = visible;
                //リサイズハンドル
                this.bResizeHangle.Visibility = (value && this.ShowResizeHandle 
                    ? Visibility.Visible 
                    : System.Windows.Visibility.Collapsed);

                setResizableCursor(value);
            }
        }

        private bool _showResizeHandle = true;
        /// <summary>ウィンドウのコンテンツ領域右下にリサイズ用ハンドルを表示するかどうか</summary>
        public bool ShowResizeHandle
        {
            get { return this._showResizeHandle; }
            set
            {
                this._showResizeHandle = value;
                this.bResizeHangle.Visibility = (value && this.Resizable
                    ? Visibility.Visible
                    : System.Windows.Visibility.Collapsed);
            }
        }

        /// <summary>ウィンドウのタイトル</summary>
        public new String Title
        {
            get { return base.Title; }
            set
            {
                labelWindowCaption.Content = value;
                base.Title = value;
            }
        }

        public UIElement WindowContent
        {
            get { return this.windowContent.Child; }
            set
            {
                this.windowContent.Child = value;
                //ウィンドウ操作のI/Fを実装していればこのウィンドウを親として通知する
                IChild ic = value as IChild;
                if (ic != null)
                { ic.ParentWindow = this; }
            }
        }

        #endregion

        #region Functions and Contance values for Window resizing

        /// <summary>
        /// リサイズモード時のリサイズ方向
        /// </summary>
        private enum ResizeDirection
        {
            None,
            Up,
            Down,
            LeftUp,
            RightUp,
            LeftDown,
            RightDown,
        };

        /// <summary>マウスボタン押下でリサイズ操作の対象とするコントロールと、そのコントロールに対するリサイズ方向を格納した辞書</summary>
        private Dictionary<object, ResizeDirection> RESIZE_HANDLE_CONTROLS;

        private readonly Dictionary<ResizeDirection, Cursor> RESIZE_CURSOR_TYPE = new Dictionary<ResizeDirection, Cursor>()
        {
            {ResizeDirection.LeftUp, Cursors.SizeNWSE},
            {ResizeDirection.Up, Cursors.SizeNS},
            {ResizeDirection.RightUp, Cursors.SizeNESW},
            {ResizeDirection.LeftDown, Cursors.SizeNESW},
            {ResizeDirection.Down, Cursors.SizeNS},
            {ResizeDirection.RightDown, Cursors.SizeNWSE},
            {ResizeDirection.None, Cursors.Arrow},
        };

        /// <summary>ウィンドウがリサイズ可能な最小サイズ</summary>
        private readonly Point MINIMUM_WINDOW_SIZE = new Point(320, 240);

        /// <summary>リサイズ開始時点でのマウスポインタのスクリーン座標</summary>
        private Point _dragStartPoint;
        /// <summary>リサイズ開始時点でのウィンドウの左上座標</summary>
        private Point _dragStartWindowLocation;
        /// <summary>リサイズ開始時点でのウィンドウの幅と高さ</summary>
        private Point _dragStartWindowSize;
        /// <summary>リサイズ方向</summary>
        private ResizeDirection _resizeDirection;

        /// <summary>
        /// リサイズ対象のコントロールにマウスオーバーした時のカーソルを設定する
        /// </summary>
        /// <param name="enableResize">リサイズ可能に設定する場合はtrue</param>
        private void setResizableCursor(bool enableResize)
        {
            //コンストラクタの初期化子で設定された場合は何もしない
            //Load時にこのメソッドを明示的に呼んでいるので、そこで初期化する
            if (RESIZE_HANDLE_CONTROLS == null)
            { return; }

            foreach(object o in RESIZE_HANDLE_CONTROLS.Keys) {
                FrameworkElement ctl = o as FrameworkElement;
                if (ctl != null)
                { ctl.Cursor = (enableResize ? RESIZE_CURSOR_TYPE[RESIZE_HANDLE_CONTROLS[ctl]] : Cursors.Arrow); }
            }
        }

        /// <summary>
        /// 指定した方向へのリサイズを開始する
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="mode"></param>
        private void StartResize(Point startPoint, ResizeDirection mode)
        {
            //今リサイズ中であれば何もせずに終了(普通はありえないが…)
            if (_resizeDirection != ResizeDirection.None)
            { return; }

            _dragStartPoint = PointToScreen(startPoint);
            _dragStartWindowLocation = new Point(this.Left, this.Top);
            _dragStartWindowSize = new Point(this.Width, this.Height);
            _resizeDirection = mode;
            this.Cursor = RESIZE_CURSOR_TYPE[mode];
            this.CaptureMouse();
        }

        /// <summary>
        /// ウィンドウ端部分のドラッグによるメニューウィンドウのリサイズ処理
        /// </summary>
        /// <param name="pos">MouseMoveイベントによって送られた今回のマウスポインタの座標</param>
        private void Resizing(Point pos)
        {
            //今リサイズ中でなければ何もせずに終了する
            if (_resizeDirection == ResizeDirection.None)
            { return; }

            // Determine the new amount to scroll.
            Point screenPos = PointToScreen(pos);
            Point delta = new Point(
                screenPos.X - _dragStartPoint.X,
                screenPos.Y - _dragStartPoint.Y);

            //リサイズ方向によるウィンドウサイズ下限の調整
            //リサイズ開始時点のサイズ + リサイズ開始時点からの相対移動量 で求められる今回のウィンドウサイズが
            //最小サイズを上回る場合のみ今回のウィンドウサイズを反映する
            if (_resizeDirection == ResizeDirection.LeftUp
                || _resizeDirection == ResizeDirection.LeftDown)
            {
                if (_dragStartWindowSize.X - delta.X > MINIMUM_WINDOW_SIZE.X)
                {
                    this.Width = _dragStartWindowSize.X - delta.X;
                    this.Left = _dragStartWindowLocation.X + delta.X;
                }
            }
            if (_resizeDirection == ResizeDirection.RightUp
                || _resizeDirection == ResizeDirection.RightDown)
            {
                if (_dragStartWindowSize.X + delta.X > MINIMUM_WINDOW_SIZE.X)
                { this.Width = _dragStartWindowSize.X + delta.X; }
            }

            if (_resizeDirection == ResizeDirection.Up
                || _resizeDirection == ResizeDirection.LeftUp
                || _resizeDirection == ResizeDirection.RightUp)
            {
                if (_dragStartWindowSize.Y - delta.Y > MINIMUM_WINDOW_SIZE.Y)
                {
                    this.Height = _dragStartWindowSize.Y - delta.Y;
                    this.Top = _dragStartWindowLocation.Y + delta.Y;
                }
            }
            if (_resizeDirection == ResizeDirection.Down
                || _resizeDirection == ResizeDirection.LeftDown
                || _resizeDirection == ResizeDirection.RightDown)
            {
                if (_dragStartWindowSize.Y + delta.Y > MINIMUM_WINDOW_SIZE.Y)
                { this.Height = _dragStartWindowSize.Y + delta.Y; }
            }
        }

        /// <summary>
        /// リサイズ処理の終了
        /// </summary>
        private void EndResize()
        {
            //今リサイズ中でなければ何もせずに終了する
            if (_resizeDirection == ResizeDirection.None)
            { return; }

            _resizeDirection = ResizeDirection.None;
            this.Cursor = RESIZE_CURSOR_TYPE[ResizeDirection.None];
            this.ReleaseMouseCapture();
        }

        #endregion

        /// <summary>
        /// ウィンドウロード完了時の初期設定的な処理を行う
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //マウスボタン押下でリサイズ処理の対象とするコントロールと、リサイズ可能方向のマッピングをする
            RESIZE_HANDLE_CONTROLS = new Dictionary<object, ResizeDirection>()
            {
                {this.resizeLU, ResizeDirection.LeftUp},
                {this.resizeU, ResizeDirection.Up},
                {this.resizeRU, ResizeDirection.RightUp},
                {this.resizeLD, ResizeDirection.LeftDown},
                {this.resizeD, ResizeDirection.Down},
                {this.resizeRD, ResizeDirection.RightDown},
                {this.bResizeHangle, ResizeDirection.RightDown},
                {this.lblResizeHangle, ResizeDirection.RightDown},
            };
            setResizableCursor(this.Resizable);
            //ウィンドウタイトルのラベル部分をクリックしたらウィンドウの移動とする
            labelWindowCaption.MouseLeftButtonDown += delegate { DragMove(); };
        }

        private void Window_Activated(object sender, EventArgs e)
        { borderWindowCaption.Background = SystemColors.ActiveCaptionBrush; }

        private void Window_Deactivated(object sender, EventArgs e)
        { borderWindowCaption.Background = SystemColors.InactiveCaptionBrush; }

        private void Window_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //マウスボタン押下時に、その対象となったコントロールがリサイズ対象オブジェクトに含まれていれば
            //リサイズ処理を開始する
            if (this.Resizable && RESIZE_HANDLE_CONTROLS.ContainsKey(e.Source))
            { StartResize(e.GetPosition(this), RESIZE_HANDLE_CONTROLS[e.Source]); }
        }

        private void Window_PreviewMouseMove(object sender, MouseEventArgs e)
        { Resizing(e.GetPosition(this)); }

        private void Window_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        { EndResize(); }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public TransparentWindow()
            : base()
        { InitializeComponent(); }

        public void Maximize()
        { this.WindowState = System.Windows.WindowState.Maximized; }

        public void Minimize()
        { this.WindowState = System.Windows.WindowState.Minimized; }

        public void ResumeSize()
        { this.WindowState = System.Windows.WindowState.Normal; }

        public void Move(int x, int y)
        {
            this.Left = x;
            this.Top = y;
        }

        public void Resize(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public void Close(bool dialogResult)
        {
            this.DialogResult = dialogResult;
            this.Close();
        }

        /// <summary>
        /// ウィンドウの状態が変化したときのコントロールの調整を行う
        /// </summary>
        /// <param name="newState">新しいウィンドウの状態</param>
        private void WindowStateChanged(System.Windows.WindowState newState)
        {
            switch (newState)
            {
                case System.Windows.WindowState.Minimized:
                    //最小化時は特に何もなし
                    break;
                case System.Windows.WindowState.Maximized:
                    this.btnWindowMaximizeButton.Content = "2";
                    this.borderWindowCaption.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                    break;
                case System.Windows.WindowState.Normal:
                    this.btnWindowMaximizeButton.Content = "1";
                    this.borderWindowCaption.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                    break;
            }
        }

        /// <summary>
        /// ウィンドウ最大化/復元ボタンを押したときの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWindowMaximizeButton_Click(object sender, RoutedEventArgs e)
        {
            //最大化ボタンを押したときはボタンのキャプションを元に戻す/最大化に切り替える
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.ResumeSize();
                this.WindowStateChanged(System.Windows.WindowState.Normal);
            }
            else
            {
                this.Maximize();
                this.WindowStateChanged(System.Windows.WindowState.Maximized);
            }
        }

        /// <summary>
        /// ウィンドウ最小化ボタンを押したときの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWindowMinimizeButton_Click(object sender, RoutedEventArgs e)
        { this.Minimize(); }

        /// <summary>
        /// ウィンドウを閉じるボタンを押したときの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWindowCloseButton_Click(object sender, RoutedEventArgs e)
        { this.Close(); }
    }
}
