﻿namespace clUtils.gui
{
    /// <summary>
    /// ユーザコントロールを乗せる親ウィンドウがコントロールからの操作を受け付けるためのインターフェイス
    /// ウィンドウ位置の操作やクローズなどを行う
    /// </summary>
    public interface IParentWindow
    {
        /// <summary>
        /// ウィンドウを最大化する
        /// </summary>
        void Maximize();

        /// <summary>
        /// ウィンドウを最小化する
        /// </summary>
        void Minimize();

        /// <summary>
        /// 最大化/最小化されているウィンドウのサイズを元に戻す
        /// </summary>
        void ResumeSize();

        /// <summary>
        /// ウィンドウを移動する
        /// </summary>
        /// <param name="x">移動先ウィンドウの左上角の水平座標</param>
        /// <param name="y">移動先ウィンドウの左上角の垂直座標</param>
        void Move(int x, int y);

        /// <summary>
        /// ウィンドウサイズを変更する.
        /// クライアント領域のサイズではなく、ウィンドウ外枠のサイズである点に注意.
        /// </summary>
        /// <param name="width">サイズ変更後のウィンドウの幅</param>
        /// <param name="height">サイズ変更後のウィンドウの高さ</param>
        void Resize(int width, int height);

        /// <summary>
        /// ウィンドウを閉じる
        /// </summary>
        void Close();

        /// <summary>
        /// ウィンドウを閉じる
        /// </summary>
        /// <param name="dialogResult">このウィンドウをダイアログとして開いていた場合、呼び出し元に返されるDialogResult</param>
        void Close(bool dialogResult);
    }
}
