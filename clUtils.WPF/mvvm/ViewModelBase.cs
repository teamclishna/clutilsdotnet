﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace clUtils.mvvm
{
    /// <summary>
    /// MVVMのViewModelを担当するクラスの基底クラス.
    /// サンプルコードからの盗用である
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// コマンドのインスタンスを保持しておくためのキャッシュ
        /// </summary>
        private Dictionary<string, DelegateCommand> _commands;

        /// <summary>
        /// プロパティの変更があったときに発行されます。
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ViewModelBase()
        { this._commands = new Dictionary<string, DelegateCommand>(); }

        /// <summary>
        /// PropertyChangedイベントを発行します。
        /// </summary>
        /// <param name="propertyName">プロパティ名</param>
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var h = this.PropertyChanged;
            if (h != null)
            {
                h(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// commandNameで指定した識別子に割り当てられたDelegateCommandのインスタンスを取得して返す.
        /// まだインスタンスが生成されていなかった場合はmethodで指定したメソッドを実行するコマンドのインスタンスを生成して返す.
        /// </summary>
        /// <param name="commandName">コマンドを一意に識別するための文字列</param>
        /// <param name="method">このコマンドで実行されるメソッド</param>
        /// <returns>指定したメソッドを実行するDelegateCommandのインスタンス</returns>
        protected DelegateCommand GetDelegateCommand(string commandName, Action method)
        {
            if (!_commands.ContainsKey(commandName))
            { _commands.Add(commandName, new DelegateCommand(method)); }
            return _commands[commandName];
        }

        /// <summary>
        /// commandNameで指定した識別子に割り当てられたDelegateCommandのインスタンスを取得して返す.
        /// まだインスタンスが生成されていなかった場合はmethodで指定したメソッドを実行するコマンドのインスタンスを生成して返す.
        /// </summary>
        /// <param name="commandName">コマンドを一意に識別するための文字列</param>
        /// <param name="method">このコマンドで実行されるメソッド</param>
        /// <param name="enabler">コマンドの実行可否を判定するためのメソッド</param>
        /// <returns>指定したメソッドを実行するDelegateCommandのインスタンス</returns>
        /// <returns></returns>
        protected DelegateCommand GetDelegateCommand(string commandName, Action method, Func<bool> enabler)
        {
            if (!_commands.ContainsKey(commandName))
            { _commands.Add(commandName, new DelegateCommand(method, enabler)); }
            return _commands[commandName];
        }
    }
}
