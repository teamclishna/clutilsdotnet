﻿using System.IO;
using System.Reflection;

namespace clUtils.xml
{
    /// <summary>
    /// XMLファイルを読み込んで構造体的に格納するためのクラス.
    /// Windows依存の読み込み先メソッドを追加している.
    /// </summary>
    public class XmlReaderWindows : XmlReader
    {

        /// <summary>
        /// アセンブリ内のリソースからXMLファイルを読み込む
        /// </summary>
        /// <param name="resourceName">"&lt;アセンブリのパス&gt;.xmlファイル名" の書式で表現したリソースのパス</param>
        /// <returns>再帰的に読み込んだノードの構造情報</returns>
        public XmlNode LoadFromResource(string resourceName)
        {
            using (Stream resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            { return LoadFromStream(resourceStream); }
        }
    }
}
