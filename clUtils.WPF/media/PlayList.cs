﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;

namespace clUtils.media
{
    /// <summary>
    /// 再生対象のプレイリスト
    /// </summary>
    public class PlayList : NotifiableViewModel, ICloneable
    {
        #region Fields

        private string _playListTitle;
        private FileInfo _playListFile;
        private ObservableCollection<MediaFile> _playMediaList;

        #endregion

        /// <summary>
        /// プレイリストのタイトル
        /// </summary>
        public string Title
        {
            get { return this._playListTitle; }
            set
            {
                this._playListTitle = value;
                OnPropertyChanged("Title");
            }
        }

        /// <summary>
        /// プレイリストをファイルから読み込んだ場合、そのファイルを示すFileInfo
        /// </summary>
        public FileInfo PlayListFile
        {
            get { return this._playListFile; }
            set
            {
                this._playListFile = value;
                OnPropertyChanged("PlayListFile");
            }
        }

        /// <summary>
        /// プレイリスト
        /// </summary>
        public ObservableCollection<MediaFile> PlayMediaList
        {
            get { return this._playMediaList; }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PlayList()
        {
            this._playMediaList = new ObservableCollection<MediaFile>();
        }

        /// <summary>
        /// このオブジェクトを複製する
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            var result = new PlayList()
            {
                Title = this.Title,
                PlayListFile = this.PlayListFile,
            };
            foreach (var m in this.PlayMediaList)
            {
                result.PlayMediaList.Add(
                    new MediaFile()
                    {
                        Title = m.Title,
                        Author = m.Author,
                        File = m.File,
                        Artwork = (m.Artwork != null ? m.Artwork.Clone() : null),
                    });
            }

            return result;
        }
    }
}
