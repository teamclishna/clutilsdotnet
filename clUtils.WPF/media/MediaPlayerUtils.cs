﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Windows.Media.Imaging;

using clUtils.log;
using clUtils.util;
using clUtils.xml;

namespace clUtils.media
{
    /// <summary>
    /// 指定したファイルのタイプを示す列挙子
    /// </summary>
    public enum MediaFileTypeEnum
    {
        /// <summary>プレイリスト</summary>
        PLAYLIST,
        /// <summary>mp3ファイル</summary>
        MP3,
        /// <summary>Windows Media Audio ファイル</summary>
        WMA,
        /// <summary>ディレクトリ</summary>
        DIRECTORY,
        /// <summary>処理対象外のファイル</summary>
        OTHER,
    }

    /// <summary>
    /// メディアプレイヤーの操作に関するユーティリティクラス
    /// </summary>
    public static class MediaPlayerUtils
    {
        /// <summary>
        /// ID3ヘッダフレーム情報
        /// </summary>
        private class Id3FrameInfo
        {
            public string FrameID { get; set; }
            public int FrameSize { get; set; }
        }

        /// <summary>
        /// MP3ファイルの最低サイズ
        /// </summary>
        private static readonly uint MP3_MINIMUM_HEADER_LENGTH = 10;

        /// <summary>
        /// WMAファイルのヘッダ領域の最小サイズ
        /// (GUID + ヘッダ長 + アイテム数 + 予約*2)
        /// </summary>
        private static readonly uint WMA_MINIMUM_HEADER_LENGTH = 16 + 8 + 4 + 1 + 1;

        /// <summary>
        /// WMAファイルの Content Desription ヘッダのGUID
        /// </summary>
        private static readonly Guid GUID_CONTENT_DESCRIPTION = new Guid("{75B22633-668E-11CF-A6D9-00AA0062CE6C}");

        /// <summary>
        /// 指定したファイルパスが示すファイルパスがメディアプレイヤーの操作対象であるかどうかをチェックして返す
        /// </summary>
        /// <param name="filePathList">Drag & Drop で渡されたファイル、またはディレクトリのリスト</param>
        /// <returns>渡された配列の要素ごとにチェックした結果を格納した配列</returns>
        public static MediaFileTypeEnum[] CheckFileTypes(string[] filePathList)
        {
            return filePathList.Select((x) => { return ChecKFileType(x); }).ToArray();
        }

        /// <summary>
        /// 指定したファイルパスが示すファイルパスがメディアプレイヤーの操作対象であるかどうかをチェックして返す
        /// </summary>
        /// <param name="filePath">Drag & Drop で渡されたファイル、またはディレクトリ</param>
        /// <returns>渡されたパスの判定結果</returns>
        public static MediaFileTypeEnum ChecKFileType(string filePath)
        {
            if ((new DirectoryInfo(filePath)).Exists)
            { return MediaFileTypeEnum.DIRECTORY; }
            else
            {
                var fi = new FileInfo(filePath);
                // 存在しないファイルを指定した場合はその他扱い
                return (
                    !fi.Exists ? MediaFileTypeEnum.OTHER :
                    fi.Extension == ".mp3" ? MediaFileTypeEnum.MP3 :
                    fi.Extension == ".wma" ? MediaFileTypeEnum.WMA :
                    fi.Extension == ".wpl" ? MediaFileTypeEnum.PLAYLIST :
                    MediaFileTypeEnum.OTHER);
            }
        }

        /// <summary>
        /// Windows Media Player のプレイリストを読み込んで返す
        /// </summary>
        /// <param name="wplFile">プレイリストのファイル</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>
        /// 読み込んだファイルを格納したプレイリスト.
        /// リスト中のメディアに存在しないファイルが含まれた場合はこれをスキップする.
        /// </returns>
        public static PlayList LoadPlayList(FileInfo wplFile, ILogger logger)
        {
            logger.PrintPush(LogLevel.DEBUG, $"start load playlist {wplFile.FullName}");
            var result = new PlayList() { PlayListFile = wplFile, };

            var reader = new XmlReader();
            var wplFileURI = new Uri(wplFile.FullName);
            var nodes = reader.LoadFromFile(wplFile.FullName);

            // プレイリストのタイトルを設定
            var titleNode = nodes.FindNode(new string[] { "head", "title" });
            if (titleNode != null)
            { result.Title = titleNode.Text; }

            foreach (var media in nodes.FindNodes(new string[] { "body", "seq", "media" }))
            {
                // プレイリストの場所を起点にした絶対パスに変換する
                var mediaPath = media.Attributes["src"];
                var absPath = new Uri(wplFileURI, mediaPath);
                logger.Print(LogLevel.DEBUG, $"{absPath.LocalPath}");

                var mediaFileInfo = new FileInfo(absPath.LocalPath);
                if (mediaFileInfo.Exists)
                {
                    logger.Print(LogLevel.DEBUG, $"add media file {mediaFileInfo.FullName}");
                    switch (MediaPlayerUtils.ChecKFileType(mediaFileInfo.FullName))
                    {
                        case MediaFileTypeEnum.MP3:
                            result.PlayMediaList.Add(MediaPlayerUtils.DetectMP3File(mediaFileInfo, logger));
                            break;

                        case MediaFileTypeEnum.WMA:
                            result.PlayMediaList.Add(MediaPlayerUtils.DetectWMAFile(mediaFileInfo, logger));
                            break;
                    }
                }
                else
                {
                    logger.Print(LogLevel.DEBUG, $"not exists media file {mediaFileInfo.FullName}");
                }
            }

            logger.PrintPop();
            return result;
        }

        /// <summary>
        /// 指定したmp3ファイルの内容を解析してタイトル、アートワークをロードして返す
        /// </summary>
        /// <param name="src">読み込み対象のファイル</param>
        /// <param name="logger">ロガーオブジェクト</param>
        /// <returns>mp3ファイルのタイトル、アートワークを格納したオブジェクト</returns>
        public static MediaFile DetectMP3File(FileInfo src, ILogger logger)
        {
            logger.PrintPush(LogLevel.DEBUG, $"detect mp3 file [{src.FullName}]");

            var fileSize = src.Length;
            if (fileSize < MP3_MINIMUM_HEADER_LENGTH)
            {
                logger.PrintPop();
                throw new InvalidDataException($"{src.FullName} is smaller than minimum size of mp3.");
            }

            var result = new MediaFile() { File = src };

            using (var stream = new FileStream(src.FullName, FileMode.Open))
            {
                int offset = 0;

                // 先頭10バイトを読み込む
                byte[] headerBytes = new byte[10];
                var readSize = stream.Read(headerBytes, offset, 10);

                logger.Print(LogLevel.VERBOSE, $"mp3 header. {headerBytes[0]:x}, {headerBytes[1]:x}, {headerBytes[2]:x}");
                logger.Print(LogLevel.VERBOSE, $"ID3 tag version. {headerBytes[3]:x}, {headerBytes[4]:x}");
                logger.Print(LogLevel.VERBOSE, $"ID3 tag flags. {headerBytes[5]:x}");

                var headerSize = SyncSafe(headerBytes, 6);
                logger.Print(LogLevel.VERBOSE, $"ID3 header size. {headerSize}");

                offset += readSize;

                // 拡張ヘッダがあれば拡張ヘッダを読み込む
                if ((headerBytes[5] & 0x20) != 0)
                {
                    byte[] extHeaderBytes = new byte[6];
                    readSize = stream.Read(extHeaderBytes, 0, 6);

                    var extHeaderSize = SyncSafe(extHeaderBytes, 0);
                    logger.Print(LogLevel.VERBOSE, $"ID3 extend header size. {extHeaderSize}");

                    offset += readSize;
                }

                while (true)
                {
                    Id3FrameInfo frame = null;

                    // ID3フレームを読み込む
                    if (3 <= headerBytes[3])
                    {
                        // ver2.3以降
                        byte[] frameBytes = new byte[10];
                        readSize = stream.Read(frameBytes, 0, 10);

                        // フレームIDとして有効であれば格納する
                        if (IsValidFrameID(frameBytes))
                        {
                            var frameSize = (
                                headerBytes[3] == 3 ? (frameBytes[4] << 24) + (frameBytes[5] << 16) + (frameBytes[6] << 8) + frameBytes[7] :
                                SyncSafe(frameBytes, 4));

                            frame = new Id3FrameInfo()
                            {
                                FrameID = new string(new char[] { (char)frameBytes[0], (char)frameBytes[1], (char)frameBytes[2], (char)frameBytes[3], }),
                                FrameSize = (int)frameSize,
                            };

                            logger.Print(LogLevel.VERBOSE, $"ID3 Header Frame: [{frame.FrameID}], size {frame.FrameSize}.");
                            offset += readSize;
                        }
                        else
                        { break; }
                    }

                    if (frame != null)
                    {
                        // 解析対象のフレームなら解析を試みる
                        switch (frame.FrameID)
                        {
                            case "TIT2":
                                result.Title = ParseTIT2(stream, frame.FrameSize);
                                break;
                            case "TPE1":
                                result.Author = ParseTPE1(stream, frame.FrameSize);
                                break;
                            case "APIC":
                                var bmp = ParseAPIC(stream, frame.FrameSize);
                                if (bmp != null)
                                { result.Artwork = bmp; }
                                break;
                            default:
                                // フレームデータの解析を行わない場合はフレームデータのサイズ分読み飛ばす
                                stream.Seek(frame.FrameSize, SeekOrigin.Current);
                                break;
                        }
                        offset += frame.FrameSize;
                    }
                }
            }
            logger.PrintPop();
            return result;
        }

        /// <summary>
        /// 指定したバイト列の先頭4バイトがフレームIDとして有効かどうかを判定する
        /// </summary>
        /// <param name="bytes">判定対象のバイト列</param>
        /// <returns>有効なフレームIDであればtrue</returns>
        private static bool IsValidFrameID(byte[] bytes)
        {
            return
                IsValidFrameIDCharacter(bytes[0]) &&
                IsValidFrameIDCharacter(bytes[1]) &&
                IsValidFrameIDCharacter(bytes[2]) &&
                IsValidFrameIDCharacter(bytes[3]);
        }

        /// <summary>
        /// 指定したバイトがフレームIDに使用可能な文字かどうかを判定する
        /// </summary>
        /// <param name="b"></param>
        /// <returns>使用可能文字であればtrue</returns>
        private static bool IsValidFrameIDCharacter(byte b)
        { return (('0' <= b && b <= '9') || ('A' <= b && b <= 'Z')); }

        /// <summary>
        /// SyncSafe型式でバイト列を数値に変換する
        /// </summary>
        /// <param name="src">読み込み対象のバイト列</param>
        /// <param name="offset">バイト列中の読み込み開始位置</param>
        /// <returns>変換した数値</returns>
        public static long SyncSafe(byte[] src, int offset)
        { return (src[offset + 0] << 21) + (src[offset + 1] << 14) + (src[offset + 2] << 7) + src[offset + 3]; }

        /// <summary>
        /// TIT2ヘッダ (曲名) を読み込んで文字列に変換して返す
        /// </summary>
        /// <param name="s">読み込み対象のStream</param>
        /// <param name="size">文字列のサイズ</param>
        /// <returns>読み込んだ文字列</returns>
        private static string ParseTIT2(Stream s, int size)
        { return ParseStringTag(s, size); }

        /// <summary>
        /// TPE1ヘッダ (アーティスト名) を読み込んで文字列に変換して返す
        /// </summary>
        /// <param name="s">読み込み対象のStream</param>
        /// <param name="size">文字列のサイズ</param>
        /// <returns>読み込んだ文字列</returns>
        private static string ParseTPE1(Stream s, int size)
        { return ParseStringTag(s, size); }

        /// <summary>
        /// 文字列型のタグを読み込んで返す
        /// </summary>
        /// <param name="s">読み込み対象のStream</param>
        /// <param name="size">文字列のサイズ</param>
        /// <returns>読み込んだ文字列</returns>
        private static string ParseStringTag(Stream s, int size)
        {
            byte[] encodingFlg = new byte[1];
            byte[] bytes = new byte[size - 1];

            // 最初の1バイトはエンコーディング指定
            var readSize = s.Read(encodingFlg, 0, 1);
            readSize = s.Read(bytes, 0, size - 1);

            try
            {
                // エンコーディング0は本来はUTF-8だが、
                // WMP でエンコードしたSJISのヘッダも0が設定されているので自動判別を試みる
                switch (encodingFlg[0])
                {
                    case 0:
                        return EncodingUtils
                            .DetectJapanese(bytes, Encoding.UTF8)
                            .GetString(bytes);
                    case 1:
                        return Encoding.Unicode.GetString(bytes);
                    default:
                        return EncodingUtils
                            .DetectJapanese(bytes, Encoding.UTF8)
                            .GetString(bytes);
                }
            }
            catch (Exception)
            { return null; }
        }

        /// <summary>
        /// APICヘッダを読み込み、カバーアートの画像を返す
        /// </summary>
        /// <param name="s"></param>
        /// <param name="size"></param>
        private static BitmapImage ParseAPIC(Stream s, int size)
        {
            byte[] bytes = new byte[size];
            var readSize = s.Read(bytes, 0, size);

            // 先頭バイトは文字コードの指定なので飛ばす
            int offset = 1;
            // MIME
            var terminated = SeekNull(bytes, offset);
            var mimeType = (new UTF8Encoding(false)).GetString(bytes, offset, terminated - offset);
            offset = terminated + 1;
            // 画像のタイプ
            var pictureType = bytes[offset];
            offset++;
            // 説明文
            terminated = SeekNull(bytes, offset);
            var description = (offset < terminated ? (new UTF8Encoding(false)).GetString(bytes, offset, terminated - offset) : "");
            offset = terminated + 1;

            var imageBytes = new byte[size - offset];
            Array.Copy(bytes, offset, imageBytes, 0, size - offset);
            using (var ms = new MemoryStream(imageBytes))
            {
                // 画像の生成に失敗した (対応していない画像フォーマットだった場合など) はnullを返す
                try
                {
                    var bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.CacheOption = BitmapCacheOption.OnLoad;
                    bmp.StreamSource = ms;
                    bmp.EndInit();
                    return bmp;
                }
                catch (Exception)
                { return null; }
                finally
                { ms.Close(); }
            }
        }

        /// <summary>
        /// 指定位置から順に走査し、nullが現れる位置のオフセットを返す
        /// </summary>
        /// <param name="bytes">走査対象のバイト列</param>
        /// <param name="start">操作開始位置</param>
        /// <returns>nullの位置のインデックス. nullが見つからなかった場合は負の値を返す.</returns>
        private static int SeekNull(byte[] bytes, int start)
        {
            int current = start;
            while (current < bytes.Length)
            {
                if (bytes[current] == 0x00)
                { return current; }
                current++;
            }
            return -1;
        }

        /// <summary>
        /// WMAファイルの情報を解析する
        /// </summary>
        /// <param name="src"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static MediaFile DetectWMAFile(FileInfo src, ILogger logger)
        {
            logger.PrintPush(LogLevel.DEBUG, $"detect wma file [{src.FullName}]");

            var fileSize = src.Length;
            if (fileSize < WMA_MINIMUM_HEADER_LENGTH)
            {
                logger.PrintPop();
                throw new InvalidDataException($"{src.FullName} is smaller than minimum size of wma.");
            }

            var result = new MediaFile() { File = src };
            using (var stream = new FileStream(src.FullName, FileMode.Open))
            {
                int offset = 0;

                // ヘッダ領域を読み込む
                byte[] headerBytes = new byte[WMA_MINIMUM_HEADER_LENGTH];
                var readSize = stream.Read(headerBytes, 0, (int)WMA_MINIMUM_HEADER_LENGTH);
                byte[] headerIDBytes = new byte[16];

                Array.Copy(headerBytes, headerIDBytes, 16);
                var headerID = new Guid(headerIDBytes);

                var objectSize = (uint)headerBytes[16] + (((uint)headerBytes[17]) << 8) + (((uint)headerBytes[18]) << 16) + (((uint)headerBytes[19]) << 24);
                var totalObjectCount = (uint)headerBytes[24] + (((uint)headerBytes[25]) << 8);

                // ヘッダ内に格納されているアイテムを読み込む
                // 最初に24バイト (GUID+オブジェクトサイズ)を読み込み、続けて残りを読み込む
                int readObjectCount = 1;
                while (readObjectCount < totalObjectCount)
                {
                    readSize = stream.Read(headerBytes, 0, 16 + 8);
                    offset += readSize;

                    var objectIDBytes = new byte[16];
                    Array.Copy(headerBytes, objectIDBytes, 16);
                    var objectID = new Guid(objectIDBytes);
                    objectSize = (uint)headerBytes[16] + (((uint)headerBytes[17]) << 8) + (((uint)headerBytes[18]) << 16) + (((uint)headerBytes[19]) << 24);

                    logger.Print(LogLevel.VERBOSE, $"object id: {objectID}");
                    logger.Print(LogLevel.VERBOSE, $"object size: {objectSize}");

                    if (24 < objectSize)
                    {
                        var dataBytes = new byte[objectSize - 24];
                        readSize = stream.Read(dataBytes, 0, (int)(objectSize - 24));

                        if (GUID_CONTENT_DESCRIPTION == objectID)
                        {
                            // Content Description ヘッダが現れたらその中から曲名、作成者の情報を取得する
                            // また、このファイルのあるディレクトリ中にある最大サイズの画像を取得し、これをアートワークとする。

                            // 曲名と作成者の長さ
                            var titleLength = (uint)dataBytes[0] + (((uint)dataBytes[1]) << 8);
                            var authorLength = (uint)dataBytes[2] + (((uint)dataBytes[3]) << 8);

                            var titleBytes = new byte[titleLength];
                            Array.Copy(dataBytes, 2 + 2 + 6, titleBytes, 0, titleLength);
                            var authorBytes = new byte[authorLength];
                            Array.Copy(dataBytes, 2 + 2 + 6 + titleLength, authorBytes, 0, authorLength);

                            var encoder = new UnicodeEncoding();
                            result.Title = encoder.GetString(titleBytes);
                            result.Author = encoder.GetString(authorBytes);

                            logger.Print(LogLevel.VERBOSE, $"title: {result.Title}");
                            logger.Print(LogLevel.VERBOSE, $"author: {result.Author}");

                            // アートワークを読み込む
                            result.Artwork = LoadArtwork(src.Directory, logger);
                        }
                        offset += (int)(objectSize - 24);
                    }
                    readObjectCount++;
                }
            }

            logger.PrintPop();
            return result;
        }

        /// <summary>
        /// 指定したディレクトリ内にある画像ファイルを1個取得し、アートワークとして返す
        /// </summary>
        /// <param name="di">検索対象のディレクトリ</param>
        /// <returns>
        /// 指定したディレクトリ内にある最大サイズの画像ファイル.
        /// 画像ファイルが存在しなかった場合は null を返す.
        /// </returns>
        private static BitmapImage LoadArtwork(
            DirectoryInfo di,
            ILogger logger)
        {
            var artworkFile = di.GetFiles()
                .Where((x) => { return (x.Extension == ".jpg" || x.Extension == ".png"); })
                .OrderByDescending(x => x.Length)
                .FirstOrDefault();

            if (artworkFile != null)
            {
                logger.Print(LogLevel.VERBOSE, $"artwork from file {artworkFile.FullName}");
                try
                {
                    var img = new BitmapImage();
                    img.BeginInit();
                    img.UriSource = new Uri(artworkFile.FullName);
                    img.EndInit();
                    return img;
                }
                catch (Exception)
                { return null; }
            }
            else
            {
                logger.Print(LogLevel.VERBOSE, $"image file for artwork not found.");
                return null;
            }
        }
    }
}

